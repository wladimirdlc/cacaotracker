package com.dlcit.etracktiva;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.FrameLayout;


public class AddProducerActivity extends AppCompatActivity   {

    private FrameLayout mFrameLayoutPrincipal;
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment selectedFragment = null;
            //selectedFragment = new NewFragment();
            switch (item.getItemId()) {
                case R.id.navigation_register_addproducer:
                    selectedFragment = new NewProducerFragment();
                    String [] farmerData = getIntent().getExtras().getStringArray("farmerdata");
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("farmerdata", farmerData);
                    selectedFragment.setArguments(bundle);
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_addproducer_container,
                            selectedFragment).commit();
                    return true;
                case R.id.navigation_edit_addproducer:
                    selectedFragment = new EditProducerFragment();
                    Bundle bundle2 = new Bundle();
                    bundle2.putInt("why_i_come", 1);
                    selectedFragment.setArguments(bundle2);
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_addproducer_container,
                            selectedFragment).commit();
                    return true;

            }
            //getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
            //      selectedFragment).commit();
            return false;
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addproducer);
        mFrameLayoutPrincipal = (FrameLayout)findViewById(R.id.fragment_addproducer_container);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation_addproducer);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        int intentFragment = getIntent().getExtras().getInt("frgToLoad");
        int why_i_come = getIntent().getExtras().getInt("why_i_come");
        String [] farmerData = getIntent().getExtras().getStringArray("farmerdata");
        Fragment selectedFragment = null;
        switch (intentFragment){
            case 1:
                selectedFragment = new NewProducerFragment();
                Bundle bundle = new Bundle();
                bundle.putSerializable("farmerdata", farmerData);
                selectedFragment.setArguments(bundle);
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_addproducer_container,
                        selectedFragment).commit();
                break;
            case 2: // Fragment See Registers
                selectedFragment = new EditProducerFragment();
                Bundle bundle2 = new Bundle();
                bundle2.putInt("why_i_come", why_i_come);
                selectedFragment.setArguments(bundle2);
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_addproducer_container,
                        selectedFragment).commit();
                break;

        }
    }

}
