package com.dlcit.etracktiva;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.dlcit.etracktiva.helper.UPA;

import org.osmdroid.api.IMapController;
import org.osmdroid.config.Configuration;
import org.osmdroid.events.MapEventsReceiver;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapController;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.MapEventsOverlay;
import org.osmdroid.views.overlay.Marker;
import org.osmdroid.views.overlay.Overlay;

import java.util.ArrayList;
import java.util.List;

public class MapActivity extends AppCompatActivity {
    private MapView         map;
    private MapController   mMapController;
    public List<UPA> allupas;
    Context c;
    TextView mUpasAdded;
    Spinner varideadSpinner;
    TextView mAreaUPA;
    GeoPoint lp;
    Button mOkaddupabtn;
    int savedUpas;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        //load/initialize the osmdroid configuration, this can be done
        final Context ctx = getApplicationContext();
        c = this; savedUpas = 0;
        allupas = new ArrayList<UPA>(); // inizializing upas
        Configuration.getInstance().load(ctx, PreferenceManager.getDefaultSharedPreferences(ctx));
        mUpasAdded = findViewById(R.id.txtupasadded);
        mUpasAdded.setText(Integer.toString(allupas.size()));
        mAreaUPA = findViewById(R.id.txtareaupa);
        mOkaddupabtn = findViewById(R.id.btnokaddupa);
        varideadSpinner = findViewById(R.id.spinnervariedadupa);
        ArrayAdapter<CharSequence> adapter4 = ArrayAdapter.createFromResource(this,
                R.array.variedad, android.R.layout.simple_spinner_item);
        varideadSpinner.setAdapter(adapter4);
        //setting this before the layout is inflated is a good idea
        //it 'should' ensure that the map has a writable location for the map cache, even without permissions
        //if no tiles are displayed, you can try overriding the cache path using Configuration.getInstance().setCachePath
        //see also StorageUtils
        //note, the load method also sets the HTTP User Agent to your application's package name, abusing osm's tile servers will get you banned based on this string
        //inflate and create the map
        map = (MapView) findViewById(R.id.mapView);
        map.setTileSource(TileSourceFactory.MAPNIK);
        map.setBuiltInZoomControls(true);
        map.setMultiTouchControls(true);
        IMapController mapController = map.getController();
        mapController.setZoom(15);
        GeoPoint startPoint = new GeoPoint(0.1272 , -79.253139); //TODO: set according to the current position
        mapController.setCenter(startPoint);
        MapEventsReceiver mReceive = new MapEventsReceiver() {
            @Override
            public boolean singleTapConfirmedHelper(GeoPoint p) {
                return false;
            }
            @Override
            public boolean longPressHelper(GeoPoint p) {
                setMarkerPosition(p);
                lp = p;
                return false;
            }
        };

        MapEventsOverlay OverlayEvents = new MapEventsOverlay(getBaseContext(), mReceive);
        map.getOverlays().add(OverlayEvents);
        Button mClearMapButton = (Button)findViewById(R.id.btndelmarkers);
        mClearMapButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                for (int i =0 ;i < 10; i++){ cleanMap(map);} // For some reason not all markers are removed at once
            }
        });
        Button mUsethislocButton = (Button)findViewById(R.id.btnusethisloc);
        mUsethislocButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(c);
                builder.setMessage("¿Está seguro de agregar esta UPA?").setPositiveButton("Si", dialogClickListener)
                        .setNegativeButton("No", dialogClickListener).show();
            }
        });
        mOkaddupabtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Go back with all saved upas
                Intent intent = new Intent(c, AddProducerActivity.class);
                intent.putExtra("frgToLoad", 1); // Value 1 to go to new register
                Bundle bundle = new Bundle();
                ArrayList<UPA> upaslist = new ArrayList<UPA>();
                for (int i = 0 ; i < allupas.size();i++){ //Populating new ArrayList
                    upaslist.add(allupas.get(i));
                }
                bundle.putParcelableArrayList("upaslist", upaslist);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
    }

    public void onResume(){
        super.onResume();
        //this will refresh the osmdroid configuration on resuming.
        //if you make changes to the configuration, use
        //SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        //Configuration.getInstance().load(this, PreferenceManager.getDefaultSharedPreferences(this));
        map.onResume(); //needed for compass, my location overlays, v6.0.0 and up
    }

    public void onPause(){
        super.onPause();
        //this will refresh the osmdroid configuration on resuming.
        //if you make changes to the configuration, use
        //SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        //Configuration.getInstance().save(this, prefs);
        map.onPause();  //needed for compass, my location overlays, v6.0.0 and up
    }
//enables this opt in feature
    private void setMarkerPosition(GeoPoint p) {
        //build the marker
        Marker m = new Marker(map);
        //m.setTextLabelBackgroundColor(backgroundColor);
        m.setIcon(null);
        m.setPosition(p);
        map.getOverlays().add(m);
    }

    private void cleanMap(MapView map){
        allupas = new ArrayList<UPA>(); // cleaning all upas
        savedUpas = 0;
        mUpasAdded.setText(Integer.toString(savedUpas));
        List<Overlay> overlayslist = map.getOverlays();
        for (int i = 0; i < overlayslist.size(); i++) {
            if (overlayslist.get(i) instanceof Marker){
             map.getOverlays().remove(overlayslist.get(i));
            }
        }
        varideadSpinner.setSelection(0);
        mAreaUPA.setText("");
        map.invalidate();
    }
    private void createUPAandGoBack(MapView map){
        // I will create all the data structure for the UPA and go back
        // Add the current upa
        String lat = Double.toString(lp.getLatitude());
        String lon = Double.toString(lp.getLongitude());
        String variety = varideadSpinner.getSelectedItem().toString();
        String area = mAreaUPA.getText().toString();
        String idupa = generateUniqueId();
        Long tsLong = System.currentTimeMillis()/1000;
        String ts =tsLong.toString();
        UPA upa = new UPA();
        upa.setValues(lat, lon,idupa,variety,ts,area,"","");
        allupas.add(upa);// Adding upa
        savedUpas ++;
        mUpasAdded.setText(Integer.toString(savedUpas));
    }
    DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            switch (which){ // We have internet, so we save both locally and remotely
                case DialogInterface.BUTTON_POSITIVE:
                    createUPAandGoBack(map);
                    break;
                case DialogInterface.BUTTON_NEGATIVE:
                    //No button clicked
                    break;
            }
        }
    };

    private String generateUniqueId(){
        Long tsLong = System.currentTimeMillis()/1000;
        String ts =tsLong.toString();
        return "UPA_" + tsLong.toString();
    }

    public List<UPA> getallupaData(){
        return allupas;
    }

}
