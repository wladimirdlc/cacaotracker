package com.dlcit.etracktiva.helper;

import android.net.Uri;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.util.Map;

public class HttpJsonParser_new {

    static InputStream is = null;
    static JSONObject jObj = null;
    static String json = "";
    HttpURLConnection urlConnection = null;

    // function get json from url
    // by making HTTP POST or GET method
    public JSONObject makeHttpRequest(String url, String method,
                                      JSONObject params) {
        JSONObject jObj = new JSONObject();

        try {
            URL urlObj = new URL(url);
            HttpURLConnection conn = (HttpURLConnection) urlObj.openConnection();
            conn.setRequestMethod(method);
            conn.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
            conn.setRequestProperty("Accept", "application/json");
            conn.setDoOutput(true);
            conn.setDoInput(true);
            DataOutputStream os = new DataOutputStream(conn.getOutputStream());
            os.writeBytes(params.toString());
            os.flush();
            os.close();
            String status = String.valueOf(conn.getResponseCode());
            String resp_msg = conn.getResponseMessage();
            InputStreamReader streamReader;
            if (conn.getResponseCode() < HttpURLConnection.HTTP_BAD_REQUEST) {
                streamReader = new InputStreamReader(conn.getInputStream());
            } else {
                streamReader = new InputStreamReader(conn.getErrorStream());
            }
            BufferedReader bufferedReader = new BufferedReader(streamReader);
            String response = null;
            StringBuilder stringBuilder = new StringBuilder();
            while ((response = bufferedReader.readLine()) != null) {
                stringBuilder.append(response + "\n");
            }
            bufferedReader.close();
            conn.disconnect();
            json = stringBuilder.toString();
            jObj.put("response_code", status);
            jObj.put("response_msg", resp_msg);
            jObj.put("msg_body", json);

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            Log.e("JSON Parser", "Error parsing data " + e.toString());
        }
        // return JSON String
        return jObj;
    }
}