package com.dlcit.etracktiva.helper;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class UPA implements Parcelable {
    public String latitude;
    public String longitude;
    public String id;
    public String variedad;
    public String timestamp;
    public String area;
    public String keyname;
    public String owner;

    public void setValues(String lat, String lon, String id_, String var, String ts ,String ar, String key, String own){
        latitude = lat;
        longitude =lon;
        id = id_;
        variedad = var;
        timestamp = ts;
        area = ar;
        keyname = key;
        owner = own;
    }
    public UPA() {
        latitude = "-999";
        longitude = "-999";
        id = "-999";
        variedad = "N.A";
        timestamp = "-999";
        area = "-999";
        keyname = "SinNombre";
        owner = "Guest";
    }
    public void setOwner(String ow){
        owner = ow;
    }
    public List<String> getListUpaValues(){
        List<String> allUPadata;
        allUPadata = new ArrayList<String>();
        allUPadata.add(latitude); //0
        allUPadata.add(longitude); //1
        allUPadata.add(id);//2
        allUPadata.add(variedad);//3
        allUPadata.add(timestamp);//4
        allUPadata.add(area);//5
        allUPadata.add(keyname);//6
        allUPadata.add(owner);//7
        return allUPadata;
    }

    public double getLatitude(){
        return Double.valueOf(latitude);
    }
    public double getLongitude() {
        return Double.valueOf(longitude);
    }
        public String getID(){
        return id;
    }

    public String getKeyname(){
        return keyname;
    }

    public UPA(Parcel in) {
        super();
        readFromParcel(in);

    }

    public static final Parcelable.Creator<UPA> CREATOR = new Parcelable.Creator<UPA>() {
        public UPA createFromParcel(Parcel in) {
            return new UPA(in);
        }

        public UPA[] newArray(int size) {

            return new UPA[size];
        }

    };

    public void readFromParcel(Parcel in) {
        this.latitude = in.readString();
        this.longitude = in.readString();
        this.id = in.readString();
        this.variedad = in.readString();
        this.timestamp = in.readString();
        this.area = in.readString();

    }


    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(latitude);
        dest.writeString(longitude);
        dest.writeString(id);
        dest.writeString(variedad);
        dest.writeString(timestamp);
        dest.writeString(area);
        dest.writeString(keyname);
    }

}
