package com.dlcit.etracktiva.helper;

import android.app.ProgressDialog;
import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.dlcit.etracktiva.helper.CheckNetworkStatus;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Locale;

import static android.content.Context.LOCATION_SERVICE;

public class LocationHelper {
    String API_KEY ="723a29aed1d262e46d8f8a6b1cd485e2";
    public String response_json;
    Context con;
    ProgressDialog pd;
    Double temp_celcius;
    JSONObject all_weather;
    double altitude;
    public double getLatitude(Context c){
        LocationManager lm = (LocationManager) c.getSystemService(LOCATION_SERVICE);
        Double lat;
        try {
            Location location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (location == null){
                location = lm.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            }
            if (location == null){
                location = lm.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
            }
            if (location != null) {
             lat = location.getLatitude();
             return lat;
            }
        } catch (SecurityException e) {
            altitude = -999;
            Toast.makeText(c,"No es posible obtener localizacion GPS!!",Toast.LENGTH_LONG).show();
        }
        return -999;
    }

    public double getLongitude(Context c){
        LocationManager lm = (LocationManager) c.getSystemService(LOCATION_SERVICE);
        Double lon;
        try {
            Location location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (location == null){
                location = lm.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            }
            if (location == null){
                location = lm.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
            }
            if (location != null){
                lon = location.getLongitude();
                return  lon;
            }
        } catch (SecurityException e) {
            Toast.makeText(c,"No es posible obtener localizacion GPS!!",Toast.LENGTH_LONG).show();
        }
        return -999;
    }

    public String getCity(Context c, Double lat, Double lon){
        if (lat> -900 && lon > -900) {
            Geocoder gcd = new Geocoder(c, Locale.getDefault());
            try {
                List<Address> addresses = gcd.getFromLocation(lat, lon, 1);
                if (addresses.size() > 0) {
                    return addresses.get(0).getLocality();
                }
            } catch (IOException e) {
                Toast.makeText(c, "No es posible obtener ciudad!!", Toast.LENGTH_LONG).show();
            }
        }
        return "N.N";

    }

    public String getCountry(Context c, Double lat, Double lon){
        if (lat> -900 && lon > -900) {
            Geocoder gcd = new Geocoder(c, Locale.getDefault());
            try {
                List<Address> addresses = gcd.getFromLocation(lat, lon, 1);
                if (addresses.size() > 0) {
                    return addresses.get(0).getCountryCode();
                }
            } catch (IOException e) {
                Toast.makeText(c, "No es posible obtener pais!!", Toast.LENGTH_LONG).show();
            }
        }
        return "N.N";
    }

    private Context getContext_ (Context c){
        return c;
    }

    public void getWeather(Context c, double lat, double log){
        if (CheckNetworkStatus.isNetworkAvailable(c)){
            con = getContext_(c);
            //api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}
            String lat_ = Double.toString(lat);
            String lon_ = Double.toString(log);
            String query = "http://api.openweathermap.org/data/2.5/weather?lat=" + lat_ +"&lon="+lon_+"&APPID=" + API_KEY;
            //String query = "http://api.openweathermap.org/data/2.5/weather?q="+city+","+country +"&APPID=" + API_KEY;
            new JsonTask().execute(query);
        //new JsonTask().execute("http://api.openweathermap.org/data/2.5/weather?q=belval,LU&APPID=723a29aed1d262e46d8f8a6b1cd485e2");
        }
    }
    public void getTemperature(String s){
        try {
            JSONObject mainObject = new JSONObject(s);
            all_weather = mainObject;
            JSONObject uniObject = mainObject.getJSONObject("main");
            String temp = uniObject.getString("temp");
            Double d = Double.parseDouble(temp);
            Double d_celsius = (d - 273.15);
            temp_celcius = d_celsius;
        }catch (JSONException e){}
    }

    public JSONObject retreiveJSONweather(){
        return all_weather;
    }

    public double retrieveTempCelsius(){
        return  temp_celcius;
    }
    private class JsonTask extends AsyncTask<String, String, String> {

        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(con);
            pd.setMessage("Obteniendo localizacion, espere...");
            pd.setCancelable(true);
            pd.show();
        }

        protected String doInBackground(String... params) {
            HttpURLConnection connection = null;
            BufferedReader reader = null;
            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));
                StringBuffer buffer = new StringBuffer();
                String line = "";
                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                    Log.d("Response: ", "> " + line);   //here u ll get whole response...... :-)
                }
                response_json = buffer.toString();
                return buffer.toString();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (pd.isShowing()) {
                pd.dismiss();
                //getTemperature(result);
            }

        }
    }

    public double getAltitude(Context c){
        LocationManager lm = (LocationManager) c.getSystemService(LOCATION_SERVICE);
        LocationProvider _locationProvider;
        try {
            Location location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (location == null){
                location = lm.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                _locationProvider = lm.getProvider(LocationManager.NETWORK_PROVIDER);
            }
            if (location == null){
                location = lm.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
                _locationProvider = lm.getProvider(LocationManager.PASSIVE_PROVIDER);
            }
            if (location!=null) {
                _locationProvider = lm.getProvider(LocationManager.GPS_PROVIDER);
                _locationProvider.supportsAltitude();
                Boolean ha = location.hasAltitude();
                double altitude = location.getAltitude();
                return altitude;
            }
        } catch (SecurityException e) {
            Toast.makeText(c,"No es posible obtener altura GPS!!",Toast.LENGTH_LONG).show();
        }
        return -999;
    }


}
