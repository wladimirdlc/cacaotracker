package com.dlcit.etracktiva;

import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.dlcit.etracktiva.helper.LockableScrollView;
import com.dlcit.etracktiva.helper.UPA;

import org.osmdroid.api.IMapController;
import org.osmdroid.config.Configuration;
import org.osmdroid.events.MapEventsReceiver;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapController;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.MapEventsOverlay;
import org.osmdroid.views.overlay.Marker;
import org.osmdroid.views.overlay.Overlay;

import java.util.ArrayList;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;

public class NewProducerFragment extends Fragment{
    ArrayList<UPA> allupas;
    List<String> keys;
    private static String STRING_EMPTY = "";
    private static String STRING_NON_SELECT = "--Seleccione--";
    private static String STRING_NN = "N.N";

    // Inputs
    private EditText mCedula;
    private EditText mNombre;
    private EditText mApellido;
    private EditText mTelefono;
    private EditText mEmail;
    private EditText mFarmerCode;
    private Spinner mSpinnerTipoProductor;

    // Inputs from map section
    private MapView map;
    private MapController mMapController;
    Context c;
    TextView mUpasAdded;
    Spinner varideadSpinner;
    TextView mAreaUPA;
    GeoPoint lp;
    Button mOkaddupabtn;
    int savedUpas;
    LockableScrollView biglayout;

    // Other vars
    String lastarea;
    String lastvariety;
    String lastlatitude;
    String lastlongitude;
    String lastowner;
    Boolean updateFarmer;
    private String farmertoEditID; //this id only works when I come to edit
    GeoPoint startPoint;
    String [] farmerdata;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Bundle bundle = getArguments();
        farmerdata = (String [])bundle.getSerializable("farmerdata");
        return inflater.inflate(R.layout.fragment_new_producer, container, false);
    }
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        MyDatabase mydb = new MyDatabase(getContext(), "db_lote", null, 1);
        SQLiteDatabase db = mydb.getWritableDatabase();
        lastarea = "0"; lastvariety = STRING_NON_SELECT; lastlatitude="-999"; lastlongitude="-999";
        startPoint = new GeoPoint(0.1272 , -79.253139); //TODO: set according to the current position
        super.onViewCreated(view, savedInstanceState);
        SharedPreferences prefs = getContext().getSharedPreferences("LOGIN_DATA", MODE_PRIVATE);
        FloatingActionButton btnaddupa = (FloatingActionButton)view.findViewById(R.id.btnaddupa);
        Button btnaddfarmer = (Button) view.findViewById(R.id.btnsavefarmer);
        mUpasAdded = (TextView)view.findViewById(R.id.txtupasaddedmap);
        allupas = new ArrayList<UPA>();
        Bundle extras = getActivity().getIntent().getExtras();
        //allupas = extras.getParcelableArrayList("upaslist");
         biglayout = (LockableScrollView) view.findViewById(R.id.bigscrollalyoutaddproducer);
        biglayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                biglayout.setScrollingEnabled(true);
                return false;
            }
        });
        mCedula = (EditText) view.findViewById(R.id.txtcedula);
        mNombre = (EditText) view.findViewById(R.id.txtnombreproductor);
        mApellido = (EditText) view.findViewById(R.id.txtapellidoproductor);
        mTelefono = (EditText) view.findViewById(R.id.txttelefonoproveedor);
        mEmail = (EditText) view.findViewById(R.id.txtemailproductor);
        mFarmerCode = (EditText) view.findViewById(R.id.txtcodproductor);
        mSpinnerTipoProductor = (Spinner) view.findViewById(R.id.spinnertipoproductor);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(),
                R.array.farmertype, android.R.layout.simple_spinner_item);
        mSpinnerTipoProductor.setAdapter(adapter);
        updateFarmer = Boolean.FALSE;// By default I add a new farmer each time I visit this fragment
        mSpinnerTipoProductor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                ((TextView) parentView.getChildAt(0)).setTextColor(getResources().getColor(R.color.colorWhiteText));
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) { }
        });

        btnaddupa.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), MapActivity.class);
                startActivity(intent);
            }
        });
        btnaddfarmer.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setMessage("¿Esta seguro de guardar el nuevo productor ?").setPositiveButton(
                        "Si", dialogClickListenerConfirmaSave)
                        .setNegativeButton("No", dialogClickListenerConfirmaSave).show();
                //savefarmer();
            }
        });
        //Map functions
        final Context ctx = getContext();
        Configuration.getInstance().load(ctx, PreferenceManager.getDefaultSharedPreferences(ctx));
        map = (MapView) view.findViewById(R.id.mapViewinProducer);
        map.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                biglayout.setScrollingEnabled(false);
                return false;
            }
        });
        map.setTileSource(TileSourceFactory.MAPNIK);
        map.setBuiltInZoomControls(true);
        map.setMultiTouchControls(true);
        IMapController mapController = map.getController();
        mapController.setZoom(15);
        mapController.setCenter(startPoint);
        MapEventsReceiver mReceive = new MapEventsReceiver() {
            @Override
            public boolean singleTapConfirmedHelper(GeoPoint p) {
                // I just lock the scrollable
                biglayout.setScrollingEnabled(false);
                biglayout.fullScroll(View.FOCUS_DOWN);
                return false;
            }
            @Override
            public boolean longPressHelper(GeoPoint p) {
                if (STRING_EMPTY.equals(mCedula.getText().toString())){
                    Toast.makeText(getContext(),"Se require cedula para agregar upas",Toast.LENGTH_LONG).show();
                    return false;
                }
                biglayout.fullScroll(View.FOCUS_DOWN);
                biglayout.setScrollingEnabled(false);
                setMarkerPosition(p,Boolean.TRUE);
                lp = p;
                return false;
            }
        };

        MapEventsOverlay OverlayEvents = new MapEventsOverlay(getContext(), mReceive);
        map.getOverlays().add(OverlayEvents);
        Button mClearMapButton = (Button)view.findViewById(R.id.btncleanmap);
        mClearMapButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                cleanMap(map); // For some reason not all markers are removed at once
            }
        });
        //Set data if we come to edit
        farmertoEditID="";
        if (farmerdata!= null){
            mCedula.setText(farmerdata[0]);
            mNombre.setText(farmerdata[1]);
            mApellido.setText(farmerdata[2]);
            mTelefono.setText(farmerdata[3]);
            mEmail.setText(farmerdata[4]);
            mFarmerCode.setText(farmerdata[5]);
            farmertoEditID = farmerdata[7];
            mSpinnerTipoProductor.setSelection(0); // TODO: set according to data from string
            farmerdata = new String[6];
            // now get upas for this farmer,
            allupas = (ArrayList<UPA>) mydb.getUPAsfromFarmer(db,mCedula.getText().toString());
            // now, lets center the map on the fisrt upa and put the markers on it
            if (allupas.size() > 0 && allupas != null) {
                startPoint.setLatitude(allupas.get(0).getLatitude());
                startPoint.setLongitude(allupas.get(0).getLongitude());
                for (int i  = 0 ; i < allupas.size() ; i ++){
                    GeoPoint currentPoint = new GeoPoint(allupas.get(i).getLatitude(),allupas.get(i).getLongitude());
                    setMarkerPosition(currentPoint,Boolean.FALSE);
                }
            }
            // Now if something was edited, it should only edit not rewrite.
            updateFarmer = Boolean.TRUE;
        }

    }

    private void savefarmer(Boolean updateFarmer_) {
        MyDatabase mydb = new MyDatabase(getContext(), "db_lote", null, 1);
        SQLiteDatabase db = mydb.getWritableDatabase();
        mydb.createTableProducers(db);
        if (db != null) {
                String ced = mCedula.getText().toString();
                String nom = mNombre.getText().toString();
                String ape = mApellido.getText().toString();
                String tel = mTelefono.getText().toString();
                String emai = mEmail.getText().toString();
                String farmercode = mFarmerCode.getText().toString();
                String tipofarmer = mSpinnerTipoProductor.getSelectedItem().toString();
                // If ced was not given
                if (STRING_EMPTY.equals(mCedula.getText().toString())){
                    Toast.makeText(getContext(),"Se require cedula para el registro",Toast.LENGTH_LONG).show();
                    return;
                }
                // Check if this user was not saved before
                if (Boolean.TRUE.equals(mydb.unique_Id_was_inserted(db,ced)) && Boolean.FALSE.equals(updateFarmer_)){
                    Toast.makeText(getContext(),"Error: Numero de cedula previamiente ingresado!!!",Toast.LENGTH_LONG).show();
                    return;
                }
                if (STRING_EMPTY.equals(mEmail.getText().toString())){emai="na@na.na";}
                lastowner = ced;
                /*if (STRING_EMPTY.equals(mNombre.getText().toString())){nom="N.A";}
                if (STRING_EMPTY.equals(mApellido.getText().toString())){ape="N.A";}
                if (STRING_EMPTY.equals(mTelefono.getText().toString())){tel="N.A";}
                if (STRING_EMPTY.equals(mFarmerCode.getText().toString())){farmercode="N.A";}*/
                if (STRING_NON_SELECT.equals(mSpinnerTipoProductor.getSelectedItem().toString())){tipofarmer="N.A";}
                Long tsLong = System.currentTimeMillis()/1000;
                String ts =tsLong.toString();
                ContentValues newRegister = new ContentValues();
                newRegister.put("unique_farmer_id",generateUniquefarmerId(ced));
                newRegister.put("idcard",ced);
                newRegister.put("name",nom);
                newRegister.put("lastname",ape);
                newRegister.put("phone", tel);
                newRegister.put("email",emai);
                newRegister.put("farmercode",farmercode);
                newRegister.put("farmertype",tipofarmer);
                newRegister.put("timestamp",ts);
                // Now filling the UPA as a chain of concatenated UPAs ids. (whole data of UPAS should be svaed in their own table)
                String UPASids = new String();
                if (allupas.size()>=1){saveUPAS();}
                for (int i = 0 ; i<allupas.size();i++){
                    UPA currentUPA = allupas.get(i);
                    String upaid = currentUPA.getID();
                    UPASids = UPASids + "%" + upaid;
                }
                newRegister.put("upas",UPASids);
                newRegister.put("timestamp",ts);
                long u; u =-1;
                if (Boolean.FALSE.equals(updateFarmer_)) {
                    u = db.insert("farmers", null, newRegister);
                }
                if (Boolean.TRUE.equals(updateFarmer_)){
                    u = db.update("farmers",newRegister,"unique_farmer_id= '" + farmertoEditID + "'",null);
                }
                if (u != -1) {
                    Toast.makeText(getContext(), "Proveedor: " + ced + " guardado localmente", Toast.LENGTH_LONG).show();
                }
                else {
                    Toast.makeText(getContext(), "Algo salio mal, proveedor no ingresado", Toast.LENGTH_LONG).show();
                }
        }
    }

    private String generateUniquefarmerId(String cedula){
        Long tsLong = System.currentTimeMillis()/1000;
        String ts =tsLong.toString();
        return "FR_" + cedula + "_" + tsLong.toString();
    }
    public void onResume(){
        super.onResume();
        //this will refresh the osmdroid configuration on resuming.
        //if you make changes to the configuration, use
        //SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        //Configuration.getInstance().load(this, PreferenceManager.getDefaultSharedPreferences(this));
        map.onResume(); //needed for compass, my location overlays, v6.0.0 and up
    }

    public void onPause(){
        super.onPause();
        //this will refresh the osmdroid configuration on resuming.
        //if you make changes to the configuration, use
        //SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        //Configuration.getInstance().save(this, prefs);
        map.onPause();  //needed for compass, my location overlays, v6.0.0 and up
    }
    private void setMarkerPosition(GeoPoint p, boolean saveUPA) {
        //build the marker
        Marker m = new Marker(map);
        //m.setTextLabelBackgroundColor(backgroundColor);
        m.setIcon(null);
        m.setPosition(p);
        map.getOverlays().add(m);
        lastlongitude = Double.toString(p.getLongitude());
        lastlatitude = Double.toString(p.getLatitude());
        // Display the dialog box requiring product type and area
        if (Boolean.TRUE.equals(saveUPA)) {
            getinfoupa();
        }
    }

    private void cleanMap(MapView map){
        allupas = new ArrayList<UPA>(); // cleaning all upas
        savedUpas = 0;
        mUpasAdded.setText(Integer.toString(savedUpas));
        //mUpasAdded.setText(Integer.toString(savedUpas));
        List<Overlay> overlayslist = map.getOverlays();
        do {
            for (int i = 0; i < overlayslist.size() ; i++) {
                if (overlayslist.get(i) instanceof Marker){
                    map.getOverlays().remove(overlayslist.get(i));
                }
            }
        }while (overlayslist.size()>1);
        map.invalidate();
        Toast.makeText(getActivity(),"UPAs eliminados",Toast.LENGTH_LONG).show();
    }

    private void getinfoupa(){
        biglayout.fullScroll(View.FOCUS_DOWN);
        lastowner = mCedula.getText().toString();
        LayoutInflater li = LayoutInflater.from(getContext());
        View promptsView = li.inflate(R.layout.dialog_info_upa, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                getContext());
        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(promptsView);
        alertDialogBuilder.setTitle("Detalles del UPA");
        final EditText areainput = (EditText) promptsView.findViewById(R.id.areaupa);
        final EditText keynameinput = (EditText) promptsView.findViewById(R.id.keynameupa);
        varideadSpinner = promptsView.findViewById(R.id.spinnervariedadupa);
        ArrayAdapter<CharSequence> adapter4 = ArrayAdapter.createFromResource(getContext(),
                R.array.variedad, android.R.layout.simple_spinner_item);
        varideadSpinner.setAdapter(adapter4);
        varideadSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                ((TextView) parentView.getChildAt(0)).setTextColor(getResources().getColor(R.color.colorWhiteText));
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) { }
        });
        // set dialog message
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                biglayout.fullScroll(View.FOCUS_DOWN);
                                Long tsLong = System.currentTimeMillis()/1000;
                                String ts = tsLong.toString();
                                UPA upa = new UPA();
                                String upaid = generateUniqueId();
                                upa.setValues(lastlatitude,lastlongitude,upaid,
                                        varideadSpinner.getSelectedItem().toString(),ts,areainput.getText().toString()
                                , keynameinput.getText().toString(),lastowner);
                                // Append to the allupas array
                                allupas.add(upa);
                                savedUpas ++ ;
                                Toast.makeText(getActivity(),
                                        "UPA " + upaid + " agregado!!!",Toast.LENGTH_SHORT).show();
                                mUpasAdded.setText(Integer.toString(savedUpas));
                                return;
                            }
                        })
                .setNegativeButton("Cancelar",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                deleteLastMarker(map);
                                dialog.cancel();
                            }
                        });
        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();
        biglayout.fullScroll(View.FOCUS_DOWN);
        alertDialog.show();
        biglayout.fullScroll(View.FOCUS_DOWN);
    }


    private String generateUniqueId(){
        Long tsLong = System.currentTimeMillis()/1000;
        String ts =tsLong.toString();
        return "UPA_" + tsLong.toString();
    }

    private void deleteLastMarker(MapView map){
        List<Overlay> overlayslist = map.getOverlays();
        for (int i = overlayslist.size()-1; i >=0 ; i--) {
                if (overlayslist.get(i) instanceof Marker){
                    map.getOverlays().remove(overlayslist.get(i));
                    break;
                }
        }
         map.invalidate();
    }

    DialogInterface.OnClickListener dialogClickListenerConfirmaSave = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            switch (which){
                case DialogInterface.BUTTON_POSITIVE:
                    savefarmer(updateFarmer);
                    break;
                case DialogInterface.BUTTON_NEGATIVE:
                    break;
            }
        }
    };

    private void saveUPAS(){
        MyDatabase mydb = new MyDatabase(getContext(), "db_lote", null, 1);
        SQLiteDatabase db = mydb.getWritableDatabase();
        mydb.createTableUPAs(db);
        if (db != null){
            for (int i = 0 ; i < allupas.size();i++) {
                UPA currentUPA = allupas.get(i);
                List<String> fieldsUPA = currentUPA.getListUpaValues();
                ContentValues newRegister = new ContentValues();
                newRegister.put("unique_upa_id",fieldsUPA.get(2));
                newRegister.put("keynameupa",fieldsUPA.get(6));
                newRegister.put("latitude",fieldsUPA.get(0));
                newRegister.put("longitude",fieldsUPA.get(1));
                newRegister.put("variety", fieldsUPA.get(3));
                newRegister.put("area",fieldsUPA.get(5));
                newRegister.put("owner",fieldsUPA.get(7));
                newRegister.put("timestamp",fieldsUPA.get(4));
                newRegister.put("serverID","-1");
                long u = db.insert("upas", null, newRegister);
                if (u != -1) {
                    Toast.makeText(getContext(), "UPAS del productor guardadas", Toast.LENGTH_SHORT).show();
                }
                else {
                    Toast.makeText(getContext(), "Algo salio mal, UPA(s) no ingresado", Toast.LENGTH_SHORT).show();
                }

            }
        }

    }
}

