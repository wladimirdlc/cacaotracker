package com.dlcit.etracktiva;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class MyDatabaseEgress extends SQLiteOpenHelper {
    //TODO: are there missing fields such as price/general info???
    String sqlCreate = "CREATE TABLE IF NOT EXISTS egresos (_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
            "unique_egress_id TEXT, qrcode TEXT, typeproduct TEXT, typeclient TEXT," +
            "client_id TEXT, ammount_paid REAL, quantity REAL, last_modif TEXT, timezone TEXT, timestamp TEXT, serverID REAL)";
    public MyDatabaseEgress(Context contexto, String nombre,
                            SQLiteDatabase.CursorFactory factory, int version) {
        super(contexto, nombre, factory, version);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(sqlCreate);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int versionAnterior, int versionNueva) {
        //NOTA: Por simplicidad del ejemplo aquí utilizamos directamente la opción de
        //      eliminar la tabla anterior y crearla de nuevo vacía con el nuevo formato.
        //      Sin embargo lo normal será que haya que migrar datos de la tabla antigua
        //      a la nueva, por lo que este método debería ser más elaborado.

        //Se elimina la versión anterior de la tabla
        db.execSQL("DROP TABLE IF EXISTS procesos");
        //Se crea la nueva versión de la tabla
        db.execSQL(sqlCreate);
    }
    public void deleteTables(SQLiteDatabase db, String table){
        db.execSQL("DROP TABLE IF EXISTS " + table);
    }

    public void openConnection(SQLiteDatabase db) {
        db = this.getWritableDatabase();
    }

    public void closeConnection(SQLiteDatabase db) {
        db.close();
    }

    public void deleteProceso(SQLiteDatabase db, String proceso_id){
        db.execSQL("DELETE FROM egresos WHERE unique_process_id='" + proceso_id+"'");
    }


    public String getProcessedinLote(SQLiteDatabase db, String loteid){
        Cursor c = db.rawQuery("SELECT lote_identificador FROM procesos WHERE lote_identificador='" +
                loteid + "'", null);
        int y = c.getCount();
        c.close();
        return new Integer(y).toString();
    }

    public void deleteLastProcess(SQLiteDatabase db, String loteid){
        // First order by the last of this lote id
        Cursor c = db.rawQuery("SELECT unique_process_id FROM procesos WHERE lote_identificador='" +
                loteid + "'" + " ORDER BY last_modif DESC", null);
        String last_element = "nn";
        if (c.moveToFirst()){
            do{
                last_element = c.getString(c.getColumnIndex("unique_process_id"));
                break;
                // do what ever you want here
            }while(c.moveToNext());
        }
        c.close();
        // Now to delete the element
        deleteProceso(db,last_element);
    }
}