package com.dlcit.etracktiva;

import android.support.v4.app.Fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import android.view.View;
import android.widget.FrameLayout;

import static java.lang.Boolean.FALSE;

public class Recepcion extends AppCompatActivity {
    private static final String TAG="RecepcionActivity";
    private FrameLayout mFrameLayoutPrincipal;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment selectedFragment = null;
            //selectedFragment = new NewFragment();
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    selectedFragment = new NewFragment();
                    Bundle bundle1 = new Bundle();
                    bundle1.putBoolean("come_to_add_register",FALSE);
                    selectedFragment.setArguments(bundle1);
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                          selectedFragment).commit();
                    return true;
                case R.id.navigation_dashboard:
                    selectedFragment = new EditFragment();
                    Bundle bundle = new Bundle();
                    bundle.putInt("why_i_come",0); // I come for editing and or remove
                    selectedFragment.setArguments(bundle);
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                          selectedFragment).commit();
                    return true;
                case R.id.navigation_notifications:
                    //break;
                    return true;
            }
            //getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
              //      selectedFragment).commit();
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recepcion);
        mFrameLayoutPrincipal = (FrameLayout)findViewById(R.id.fragment_container);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        //Setting initial fragment to show/ I want add fragment...
        int intentFragment = getIntent().getExtras().getInt("frgToLoad");
        Fragment selectedFragment = null;
        switch (intentFragment){
            case 1: // Fragment New
                selectedFragment = new NewFragment();
                Bundle bundle1 = new Bundle();
                bundle1.putBoolean("come_to_add_register",FALSE);
                String [] farmerData = getIntent().getExtras().getStringArray("farmerdata");
                bundle1.putSerializable("farmerdata", farmerData);
                selectedFragment.setArguments(bundle1);
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        selectedFragment).commit();
                break;
            case 2: // Fragment Edit
                selectedFragment = new EditFragment();
                Bundle bundle2 = new Bundle();
                bundle2.putInt("why_i_come",2);// I come to see and select
                selectedFragment.setArguments(bundle2);
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        selectedFragment).commit();
                navigation.setVisibility(View.INVISIBLE); // I do not need the navigation here
                FrameLayout.MarginLayoutParams params = (FrameLayout.MarginLayoutParams) mFrameLayoutPrincipal.getLayoutParams();
                params.bottomMargin = 0; // I need more space for displaying the list comprehensive
                mFrameLayoutPrincipal.setLayoutParams(params);
                break;
                case 3: // Fragment Search Register
                // Load corresponding fragment
                break;
        }



    }

}

