package com.dlcit.etracktiva;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.dlcit.etracktiva.helper.CheckNetworkStatus;
import com.dlcit.etracktiva.helper.LocationHelper;

import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import static android.content.Context.MODE_PRIVATE;
import static android.database.sqlite.SQLiteDatabase.openDatabase;
import static android.database.sqlite.SQLiteDatabase.openOrCreateDatabase;

public class NewFragment extends Fragment {
    private Spinner mUPAList;
    private Spinner mTipoProd;
    private Spinner mVariedad;
    private Spinner mInen;
    private Button mNewButton;
    private Button mAddButton;
    private Button mSearchFarmer;
    private EditText mQuantity;
    private EditText mPrice;
    private EditText mFarmerNationalid;
    private static String STRING_EMPTY = "";
    private static String STRING_NON_SELECT = "--Seleccione--";
    private static String STRING_NN = "N.N";
    private String upa;
    private String producer;
    private String storage;
    private String type;
    private String variety;
    private String quantity;
    private String price;
    private String inen;
    private String cert;
    private String lastmodif;
    private String timezone;
    private String unique_reg_id;
    private String unique_lote_id;
    private Boolean function_add_directly;
    private Double latitude;
    private Double longitude;
    private String locality;
    private String country;
    private Double altitude;
    public JSONObject all_weather;
    public String weather2store;
    public String username;
    LocationHelper lhelper = new LocationHelper();
    String [] farmerdata;

    // Others requiered variables
    private int success;
    private ProgressDialog pDialog;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // Lets try to recover data from producer and their upas
        Bundle bundle = getArguments();
        farmerdata = (String[]) bundle.getSerializable("farmerdata");
        return inflater.inflate(R.layout.fragment_new, container, false);
    }
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        SharedPreferences prefs = getContext().getSharedPreferences("LOGIN_DATA", MODE_PRIVATE);
        username = (prefs.getString("user", "Guest"));
        mUPAList = (Spinner) view.findViewById(R.id.upalist);
        mTipoProd = (Spinner)view.findViewById(R.id.tipolista) ;
        mVariedad = (Spinner)view.findViewById(R.id.variedadspinner) ;
        mQuantity = (EditText)view.findViewById(R.id.txtqty);
        mInen = (Spinner)view.findViewById(R.id.inenspinner) ;
        mFarmerNationalid = (EditText) view.findViewById(R.id.txtproducerreceiver);
        mSearchFarmer = (Button) view.findViewById(R.id.btnsearchproducer);
        // Initialization of data input
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(),
                R.array.upas_array, android.R.layout.simple_spinner_item);
        ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(getContext(),
                R.array.tipo_producto, android.R.layout.simple_spinner_item);
        mTipoProd.setAdapter(adapter2);
        ArrayAdapter<CharSequence> adapter3 = ArrayAdapter.createFromResource(getContext(),
                R.array.variedad, android.R.layout.simple_spinner_item);
        mVariedad.setAdapter(adapter3);
        ArrayAdapter<CharSequence> adapter4 = ArrayAdapter.createFromResource(getContext(),
                R.array.inen2, android.R.layout.simple_spinner_item);
        mInen.setAdapter(adapter4);
        // Change the color of spinner list progrmatically --- I was forced to this
        mTipoProd.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                ((TextView) parentView.getChildAt(0)).setTextColor(getResources().getColor(R.color.colorWhiteText));
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) { }
        });
        mVariedad.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                ((TextView) parentView.getChildAt(0)).setTextColor(getResources().getColor(R.color.colorWhiteText));
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) { }
        });
        mInen.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                ((TextView) parentView.getChildAt(0)).setTextColor(getResources().getColor(R.color.colorWhiteText));
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) { }
        });
        mNewButton = (Button)view.findViewById(R.id.btn_new);
        mAddButton = (Button)view.findViewById(R.id.btn_add);
        mPrice = (EditText)view.findViewById(R.id.txtprecio);

        // Temporary values to test TODO: remove this values
        //mUPAList.setSelection(0);
        //mQuantity.setText("5");
        mTipoProd.setSelection(2);
        mVariedad.setSelection(2);
        mInen.setSelection(2);
        //mPrice.setText("2500");
        // Getting Geo Location, Country and City
        longitude = lhelper.getLongitude(getContext());
        latitude = lhelper.getLatitude(getContext());
        country = lhelper.getCountry(getContext(),latitude,longitude);
        locality = lhelper.getCity(getContext(),latitude,longitude);
        altitude = lhelper.getAltitude(getContext());
        if (!STRING_NN.equals(country) && !STRING_NN.equals(locality)) {
            lhelper.getWeather(getContext(), latitude, longitude);
        }
        // If we have chose  the producer lets add according what have been chosen
        if (farmerdata != null){
            mFarmerNationalid.setText(farmerdata[0]);
            List<String> spinnerArray = new ArrayList<>();
            for (int i = 1 ; i < farmerdata.length ; i ++){
                spinnerArray.add(farmerdata[i]);
            }
            ArrayAdapter<String> adapter_sp = new ArrayAdapter<String>(
                    getActivity(), android.R.layout.simple_spinner_item, spinnerArray);
            adapter_sp.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mUPAList.setAdapter(adapter_sp);
            mUPAList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                    ((TextView) parentView.getChildAt(0)).setTextColor(getResources().getColor(R.color.colorWhiteText));
                }
                @Override
                public void onNothingSelected(AdapterView<?> adapterView) { }
            });
        }

        // If we came from the fragment listivew, go directly to save
        function_add_directly = getArguments().getBoolean("come_to_add_register");
        if (Boolean.TRUE.equals(function_add_directly)) {
            unique_reg_id = generateUniqueRegId(username);
            unique_lote_id = getArguments().getString("lote_id_bundle");
            addLocally(unique_lote_id,generateUniqueRegId(username));
        }


        mNewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (CheckNetworkStatus.isNetworkAvailable(getContext())) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                    builder.setMessage("¿Está seguro de ingresar nuevo lote y registro?").setPositiveButton("Si", dialogClickListener)
                            .setNegativeButton("No", dialogClickListener).show();

                }else{
                    // Here we should handle gracefully the off line processing
                    Toast.makeText(getContext(),"Unable to connect to internet I will store it locally",Toast.LENGTH_LONG).show();
                    unique_reg_id = generateUniqueRegId(username);
                    unique_lote_id = generateLoteid(username);
                    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                    builder.setMessage("¿Está seguro de ingresar nuevo lote y registro localmente?").setPositiveButton("Si", dialogClickListener)
                            .setNegativeButton("No", dialogClickListener).show();
                }
           }
        });

        mAddButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Sending instruction for adding register to lote, it means select id and come back
                Bundle bundle = new Bundle();
                bundle.putInt("why_i_come",1); // I come for adding register to lote\
                bundle.putSerializable("farmerdata",farmerdata);
                // Launch fragment search and edit
                Fragment newFragment = new EditFragment();
                newFragment.setArguments(bundle);
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.fragment_container, newFragment);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

        mSearchFarmer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Lets open the activity and fragment with the list of farmers
                Intent intent = new Intent(getActivity(), AddProducerActivity.class);
                intent.putExtra("frgToLoad", 2); // Value 1 to go to new register egress
                intent.putExtra("why_i_come",2); // Value for say to the list select and come back
                startActivity(intent);
            }
        });


    }
    DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            switch (which){ // We have internet, so we save both locally and remotely
                case DialogInterface.BUTTON_POSITIVE:
                    // First create the unique reg_id
                    unique_reg_id = generateUniqueRegId(username);
                    unique_lote_id = generateLoteid(username);
                    // Here it should first add the register and then create the lot with that register
                    // Add register to the local database
                     addLocally(unique_lote_id, unique_reg_id);

                    break;
                case DialogInterface.BUTTON_NEGATIVE:
                    //No button clicked
                    break;
            }
        }
    };

    private String generateLoteid(String whoami){
        Long tsLong = System.currentTimeMillis()/1000;
        String ts = tsLong.toString();
        return whoami + "_" + ts;
    }
    private String generateUniqueRegId(String whoami){
        Long tsLong = System.currentTimeMillis()/1000;
        String ts =tsLong.toString();
        return "R_" + whoami + "_" + tsLong.toString();
    }
    private void addLocally(String loteid, String regid){
        // First fecth weather if possible
        weather2store = lhelper.response_json;
        if (weather2store==null){weather2store="No disponible";}
        MyDatabase mydb = new MyDatabase(getContext(),"db_lote",null,1);
        // I will write the new register
        SQLiteDatabase db = mydb.getWritableDatabase();
        // Create the table if not exists
        mydb.onCreate(db); // this Creates the resgistrostable
        // Adding data
        if(db != null) {
            upa =  mUPAList.getSelectedItem().toString();
            producer = mFarmerNationalid.getText().toString();
            storage = "provisional";
            quantity = mQuantity.getText().toString();
            variety = mVariedad.getSelectedItem().toString();
            type = mTipoProd.getSelectedItem().toString();
            inen = mInen.getSelectedItem().toString();
            cert = mInen.getSelectedItem().toString();
            TimeZone timeZone = TimeZone.getDefault();
            price = mPrice.getText().toString();
            DateFormat date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            lastmodif = date.format(Calendar.getInstance().getTime());
            Long tsLong = System.currentTimeMillis()/1000;
            String ts =tsLong.toString();
            timezone = timeZone.getDisplayName(false,TimeZone.SHORT);
            if (STRING_NON_SELECT.equals(mUPAList.getSelectedItem().toString())){upa="N.A";}
            if (STRING_EMPTY.equals(mQuantity.getText().toString())){quantity="0";}
            if (STRING_NON_SELECT.equals(mTipoProd.getSelectedItem().toString())){type="N.A";}
            if (STRING_NON_SELECT.equals(mVariedad.getSelectedItem().toString())){variety="N.A";}
            if (STRING_NON_SELECT.equals(mInen.getSelectedItem().toString())){inen="N.A";}
            if (STRING_EMPTY.equals(mPrice.getText().toString())){price="0";}
            if (STRING_NON_SELECT.equals(mFarmerNationalid.getText().toString())){
                Toast.makeText(getContext(),"Error: Id del Productor es necesario para el registro", Toast.LENGTH_LONG).show();
            }
            ContentValues nuevoRegistro = new ContentValues();
            nuevoRegistro.put("lote_identificador", loteid);
            nuevoRegistro.put("unique_reg_id", regid);
            nuevoRegistro.put("upa", upa);
            nuevoRegistro.put("producer", producer);
            nuevoRegistro.put("storage", storage);
            nuevoRegistro.put("quantity", quantity);
            nuevoRegistro.put("type", type);
            nuevoRegistro.put("variety", variety);
            nuevoRegistro.put("inen", inen);
            nuevoRegistro.put("cert", cert);
            nuevoRegistro.put("precio", price);
            nuevoRegistro.put("last_modif", lastmodif);
            nuevoRegistro.put("timezone", timezone);
            nuevoRegistro.put("latitude", latitude);
            nuevoRegistro.put("longitude", longitude);
            nuevoRegistro.put("locality", locality);
            nuevoRegistro.put("country", country);
            nuevoRegistro.put("altitude", altitude);
            nuevoRegistro.put("weather", weather2store);
            nuevoRegistro.put("timestamp", ts);
            nuevoRegistro.put("serverID","-1");

            long u = db.insert("registros", null, nuevoRegistro);
            if (u != -1) {
                Toast.makeText(getContext(), "Lote: " + unique_lote_id + " guardado localmente", Toast.LENGTH_LONG).show();
            }
            else {
                Toast.makeText(getContext(), "Algo salio mal, registro no ingresado", Toast.LENGTH_LONG).show();
            }
        }
        db.close();
    }

}

