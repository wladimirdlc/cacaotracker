package com.dlcit.etracktiva;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import android.widget.TextView;
import android.widget.Toast;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;

public class EditFragment extends Fragment {
    private SimpleCursorAdapter dataAdapter;
    private int why_i_am_here;
    private String loteid;
    private SQLiteDatabase db;
    private MyDatabase mydb;
    private TextView txtsearchLote;
    private String [] farmerdata;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        why_i_am_here = getArguments().getInt("why_i_come");
        Bundle bundle = getArguments();
        farmerdata = (String[]) bundle.getSerializable("farmerdata");
        return inflater.inflate(R.layout.fragment_edit, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        txtsearchLote = (TextView)view.findViewById(R.id.txtserachlote);
        txtsearchLote.clearFocus(); // Uncomment if you want to use this TextView
        mydb = new MyDatabase(getContext(),"db_lote",null,1);
        db = mydb.getWritableDatabase();
        Boolean tableexists = mydb.isTableExists("registros",db);
        ListView listView = (ListView) view.findViewById(R.id.listview1);
        if (FALSE.equals(tableexists)){
            Toast.makeText(getContext(), "Aun no existen datos!!!", Toast.LENGTH_LONG).show();
            return;
        }
        Cursor c = db.rawQuery("SELECT _id ,lote_identificador, upa, producer,last_modif, COUNT(*) AS C FROM registros " +
                "GROUP BY lote_identificador ORDER BY last_modif DESC", null);
        // Get Number of register per lote
        String[] columns = new String[] {
                "lote_identificador",
                "C",
                "producer",
                "last_modif"
        };
        int[] to = new int[] {
                R.id.listalote,
                R.id.listaregistros,
                R.id.listaproductor,
                R.id.listafechamodif,
        };

        dataAdapter = new SimpleCursorAdapter(getContext(),R.layout.lote_info,c,columns,to,0);
        listView.setAdapter(dataAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> listView, View view,
                                    int position, long id) {
                Cursor cursor = (Cursor) listView.getItemAtPosition(position);
                String loteid2 =
                        cursor.getString(cursor.getColumnIndexOrThrow("lote_identificador"));
                loteid = loteid2;
                // If I comme from add register to lote, in which case goes directly to store
                switch (why_i_am_here){
                    case 0: // I am here to edit/remove
                        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                        builder.setMessage("¿Desea editar o eliminar el lote " + loteid + "?").setPositiveButton("Editar",
                                dialogClickListener2)
                                .setNegativeButton("Eliminar", dialogClickListener2).show();
                        break;
                    case 1: // I am here to add register to lte
                        AlertDialog.Builder builder2 = new AlertDialog.Builder(getContext());
                        builder2.setMessage("¿Está seguro de ingresar registro en lote existente " + loteid + "?").setPositiveButton("Si", dialogClickListener)
                                .setNegativeButton("No", dialogClickListener).show();
                        break;
                    case 2: // I am here just to see, select and come back
                        Intent intent = new Intent(getActivity(), ProcessingActivity.class);
                        intent.putExtra("lote_id",loteid);
                        intent.putExtra("frgToLoad",1);
                        startActivity(intent);
                        break;
                }


            }
        });
        db.close();
        //c.close();
    }
    DialogInterface.OnClickListener dialogClickListener2 = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            switch (which){ // We have internet, so we save both locally and remotely
                case DialogInterface.BUTTON_POSITIVE:
                    // Case Editar, first display the existing registers from this lote
                    String [] regs = mydb.getRegister(mydb.getWritableDatabase(),loteid);
                    AlertDialog.Builder builder2 = new AlertDialog.Builder(getContext());
                    builder2.setTitle("Eliga registro");
                    builder2.setItems(regs, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // the user clicked on colors[which]
                        }
                    });
                    builder2.show();
                    break;
                case DialogInterface.BUTTON_NEGATIVE:
                    // Case Eliminar lote
                    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                    builder.setMessage("¿Esta seguro de eliminar el lote " + loteid + " y todos sys registros?").setPositiveButton(
                            "Si", dialogClickListenerConfirmarDelete)
                            .setNegativeButton("No", dialogClickListenerConfirmarDelete).show();
                    break;
            }
        }
    };

    DialogInterface.OnClickListener dialogClickListenerConfirmarDelete = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            switch (which){
                case DialogInterface.BUTTON_POSITIVE:
                    MyDatabase mydb = new MyDatabase(getContext(),"db_lote",null,1);
                    mydb.deleteLote(mydb.getWritableDatabase(),loteid);
                    // Refresh fragment edit
                    refreshEdit(FALSE);
                    break;
                case DialogInterface.BUTTON_NEGATIVE:
                    break;
            }
        }
    };

    DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            switch (which){ // We have internet, so we save both locally and remotely
                case DialogInterface.BUTTON_POSITIVE:
                    // Return to fragment add but with known lote_id (pass this value)
                    // Sending instruction for adding register to lote, it means select id and come back
                    Bundle bundle = new Bundle();
                    bundle.putString("lote_id_bundle",loteid);
                    bundle.putBoolean("come_to_add_register",TRUE);
                    bundle.putSerializable("farmerdata",farmerdata);
                    // Launch fragment search and edit
                    Fragment newFragment = new NewFragment();
                    newFragment.setArguments(bundle);
                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                    transaction.replace(R.id.fragment_container, newFragment);
                    transaction.addToBackStack(null);
                    transaction.commit();
                    break;
                case DialogInterface.BUTTON_NEGATIVE:
                    //No button clicked
                    break;
            }
        }
    };

    private void refreshEdit(boolean editoradd){
        Bundle bundle = new Bundle();
        bundle.putBoolean("add_register",editoradd);
        // Launch fragment search and edit
        Fragment newFragment = new EditFragment();
        newFragment.setArguments(bundle);
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, newFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

}