package com.dlcit.etracktiva;

import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.FrameLayout;


public class ProcessingActivity extends AppCompatActivity   {

    private FrameLayout mFrameLayoutPrincipal;
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment selectedFragment = null;
            //selectedFragment = new NewFragment();
            switch (item.getItemId()) {
                case R.id.navigation_camera:
                    selectedFragment = new ProcessingCameraFragment();
                    Bundle bundle1 = new Bundle();
                    bundle1.putString("lote_id",getIntent().getExtras().getString("lote_id"));
                    selectedFragment.setArguments(bundle1);
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_processing_container,
                            selectedFragment).commit();
                    return true;
                case R.id.navigation_dashboard:
                    return true;

            }
            //getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
            //      selectedFragment).commit();
            return false;
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_processing);
        mFrameLayoutPrincipal = (FrameLayout)findViewById(R.id.fragment_processing_container);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation_processing);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        int intentFragment = getIntent().getExtras().getInt("frgToLoad");
        Fragment selectedFragment = null;
        switch (intentFragment){
            case 1: // Fragment Camera
                selectedFragment = new ProcessingCameraFragment();
                Bundle bundle1 = new Bundle();
                bundle1.putString("lote_id",getIntent().getExtras().getString("lote_id"));
                selectedFragment.setArguments(bundle1);
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_processing_container,
                        selectedFragment).commit();
                break;
            case 2: // TODO: Fragment See Registers
                break;

        }
    }

}
