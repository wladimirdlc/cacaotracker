package com.dlcit.etracktiva;

import android.app.Activity;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.hardware.Camera;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import net.sourceforge.zbar.Config;
import net.sourceforge.zbar.Image;
import net.sourceforge.zbar.ImageScanner;
import net.sourceforge.zbar.Symbol;
import net.sourceforge.zbar.SymbolSet;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;

import static android.content.Context.MODE_PRIVATE;

public class NewEgressFragment extends Fragment implements Camera.PreviewCallback, ZBarConstants  {

    private static final String TAG = "ZBarScannerActivity";
    private CameraPreview mPreview;
    private Camera mCamera;
    private ImageScanner mScanner;
    private Handler mAutoFocusHandler;
    private boolean mPreviewing = true;
    private Spinner mSpinnerTipopago;
    private Spinner mTipoCliente;
    private TextView mClientId;
    private TextView mAmmountPaid;
    private TextView mPesoEgress;

    // Strings to be saved into the data base egresos
    private String QRCode;
    private String username;
    private String tipopago;
    private String clientid;
    private String uniqueegressid;
    private String ammountpaid;
    private String tipoproducto;
    private String tipoclient;
    private String last_modif;
    private String time_zone;
    private String pesoegress;

    private static String STRING_EMPTY = "";
    private static String STRING_NN = "N.N";

    private int currentprocessed;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_new_egress, container, false);
    }
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mSpinnerTipopago = (Spinner)view.findViewById(R.id.spinnertipopago);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(),
                R.array.tipo_pago_array, android.R.layout.simple_spinner_item);
        mSpinnerTipopago.setAdapter(adapter);
        mTipoCliente = (Spinner)view.findViewById(R.id.spinnertipocliente);
        adapter = ArrayAdapter.createFromResource(getContext(),
                R.array.tipo_client, android.R.layout.simple_spinner_item);
        mTipoCliente.setAdapter(adapter);
        mSpinnerTipopago.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                ((TextView) parentView.getChildAt(0)).setTextColor(getResources().getColor(R.color.colorWhiteText));
                ((TextView) parentView.getChildAt(0)).setTextSize(12);
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) { }
        });
        mTipoCliente.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                ((TextView) parentView.getChildAt(0)).setTextColor(getResources().getColor(R.color.colorWhiteText));
                ((TextView) parentView.getChildAt(0)).setTextSize(12);
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) { }
        });
        mClientId = (TextView)view.findViewById(R.id.txtclientid);
        mAmmountPaid = (TextView)view.findViewById(R.id.txtpreciopagado);
        mPesoEgress = (TextView)view.findViewById(R.id.txtpesoegress);
        SharedPreferences prefs = getContext().getSharedPreferences("LOGIN_DATA", MODE_PRIVATE);
        username = (prefs.getString("user", "Guest"));
        currentprocessed = 0;
        //CAMERA
        if(!isCameraAvailable()) {
            // Cancel request if there is no rear-facing camera.
            cancelRequest();
            return;
        }
        // Hide the window title.
        //requestWindowFeature(Window.FEATURE_NO_TITLE);
        // getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        mAutoFocusHandler = new Handler();
        // Create and configure the ImageScanner;
        setupScanner();
        // Create a RelativeLayout container that will hold a SurfaceView,
        // and set it as the content of our activity.
        mPreview = new CameraPreview(getContext(), this, autoFocusCB);
        //setContentView(mPreview);
        // Create our Preview view and set it as the content of our activity.
        FrameLayout preview = (FrameLayout) view.findViewById(R.id.camera_preview_egress);
        preview.addView(mPreview);
    }
    public void setupScanner() {
        mScanner = new ImageScanner();
        mScanner.setConfig(0, Config.X_DENSITY, 3);
        mScanner.setConfig(0, Config.Y_DENSITY, 3);
        int[] symbols = getActivity().getIntent().getIntArrayExtra(SCAN_MODES);
        if (symbols != null) {
            mScanner.setConfig(Symbol.NONE, Config.ENABLE, 0);
            for (int symbol : symbols) {
                mScanner.setConfig(symbol, Config.ENABLE, 1);
            }
        }
    }
    @Override
    public void onResume() {
        super.onResume();
        // Open the default i.e. the first rear facing camera.
        mCamera = Camera.open();
        mCamera.setDisplayOrientation(90);
        if(mCamera == null) {
            // Cancel request if mCamera is null.
            cancelRequest();
            return;
        }
        mPreview.setCamera(mCamera);
        mPreview.showSurfaceView();
        mPreviewing = true;
    }

    @Override
    public void onPause() {
        super.onPause();
        // Because the Camera object is a shared resource, it's very
        // important to release it when the activity is paused.
        if (mCamera != null) {
            mPreview.setCamera(null);
            mCamera.cancelAutoFocus();
            mCamera.setPreviewCallback(null);
            mCamera.stopPreview();
            mCamera.release();
            mPreview.hideSurfaceView();
            mPreviewing = false;
            mCamera = null;
        }
    }
    public boolean isCameraAvailable() {
        PackageManager pm = getActivity().getPackageManager();
        return pm.hasSystemFeature(PackageManager.FEATURE_CAMERA);
    }
    public void cancelRequest() {
        Intent dataIntent = new Intent();
        dataIntent.putExtra(ERROR_INFO, "Camera unavailable");
        getActivity().setResult(Activity.RESULT_CANCELED, dataIntent);
        getActivity().finish();
    }
    public void onPreviewFrame(byte[] data, Camera camera) {
        Camera.Parameters parameters = camera.getParameters();
        Camera.Size size = parameters.getPreviewSize();
        Image qrcode = new Image(size.width, size.height, "Y800");
        qrcode.setData(data);
        int result = mScanner.scanImage(qrcode);
        if (result != 0) {
            onPause();
            //mCamera.cancelAutoFocus();
            //mCamera.setPreviewCallback(null);
            //mCamera.stopPreview();
            //mPreviewing = false;
            SymbolSet syms = mScanner.getResults();
            for (Symbol sym : syms) {
                String symData = sym.getData();
                if (!TextUtils.isEmpty(symData)) {
                    String[] qrdata = symData.split("%");
                    if (qrdata.length == 3) {
                        if (getString(R.string.dlcit).equals(qrdata[1])) {
                            QRCode = qrdata[2];
                            processQR(QRCode);
                        }
                        else{showInvalidQR();}
                    }
                    else {showInvalidQR();}
                }
                else{
                    showInvalidQR();
                }
            }
        }
    }
    private Runnable doAutoFocus = new Runnable() {
        public void run() {
            if(mCamera != null && mPreviewing) {
                mCamera.autoFocus(autoFocusCB);
            }
        }
    };
    // Mimic continuous auto-focusing
    Camera.AutoFocusCallback autoFocusCB = new Camera.AutoFocusCallback() {
        public void onAutoFocus(boolean success, Camera camera) {
            mAutoFocusHandler.postDelayed(doAutoFocus, 1000);
        }
    };
    protected void showInvalidQR(){
        Toast.makeText(getContext(),"QR Code no valido!!!", Toast.LENGTH_LONG).show();
        SystemClock.sleep(500);
        onResume();
    }

    private void processQR(String qrtext){
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){ //
                    case DialogInterface.BUTTON_POSITIVE:
                        // Processing and resume
                        // Save in database
                        currentprocessed ++;
                        saveInDB();
                        onResume();
                        break;
                    case DialogInterface.BUTTON_NEGATIVE:
                        // Only resume
                        onResume();
                        break;
                }
            }
        };
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMessage("¿Está seguro de despachar este saco: " + QRCode + "?").setPositiveButton("Si", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();
    }

    private void saveInDB(){
        MyDatabaseEgress mydb = new MyDatabaseEgress(getContext(),"db_lote",null,1);
        SQLiteDatabase db = mydb.getWritableDatabase();
        //mydb.deleteTables(db,"egresos");
        mydb.onCreate(db);
        if(db != null) {
            uniqueegressid = generateUniqueEgressId(username);
            tipoproducto = "N.A"; //TODO: make sure it is not needed here
            tipoclient = mTipoCliente.getSelectedItem().toString();
            tipopago = mSpinnerTipopago.getSelectedItem().toString();
            clientid = mClientId.getText().toString();
            ammountpaid = mAmmountPaid.getText().toString();
            pesoegress = mPesoEgress.getText().toString();
            DateFormat date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            TimeZone timeZone = TimeZone.getDefault();
            last_modif = date.format(Calendar.getInstance().getTime());
            time_zone = timeZone.getDisplayName(false,TimeZone.SHORT);
            Long tsLong = System.currentTimeMillis()/1000;
            String ts =tsLong.toString();
            // checking any empty field
            if (STRING_EMPTY.equals(clientid)){clientid="N.A";}
            if (STRING_EMPTY.equals(ammountpaid)){ammountpaid="0";}
            if (STRING_EMPTY.equals(pesoegress)){pesoegress="0";}
            if ("--Tipo Pago--".equals(tipopago)){tipopago="N.A";}
            if ("--Producto--".equals(tipoproducto)){tipoproducto="N.A";}
            if ("--Cliente--".equals(tipoclient)){tipoclient="N.A";}

            // inserting into the table
            ContentValues newProcess = new ContentValues();
            newProcess.put("unique_egress_id", uniqueegressid);
            newProcess.put("qrcode", QRCode);
            //newProcess.put("typeproduct", tipoproducto);
            newProcess.put("typeproduct", tipopago); //TODO: This is provisional change the name in table!!!
            newProcess.put("typeclient", tipoclient);
            newProcess.put("client_id", clientid);
            newProcess.put("ammount_paid",ammountpaid);
            newProcess.put("quantity",pesoegress);
            newProcess.put("last_modif",last_modif);
            newProcess.put("timezone",time_zone);
            newProcess.put("timestamp",ts);
            newProcess.put("serverID","-1");
            long u = db.insert("egresos", null, newProcess);
            if (u != -1){
                Toast.makeText(getContext(), "Egresos: " + uniqueegressid + " guardado localmente!!!", Toast.LENGTH_LONG).show();
            }
            else {
                Toast.makeText(getContext(), "Algo salio mal, registro no ingresado", Toast.LENGTH_LONG).show();
            }
        }
        // Update number of processed items in this lote
        //mydb.getProcessedinEgresos(db,uniqueegressid);
        db.close();
    }

    private String generateUniqueEgressId(String whoami){
        Long tsLong = System.currentTimeMillis()/1000;
        String ts =tsLong.toString();
        return "E_" + whoami + "_" + tsLong.toString();
    }
}