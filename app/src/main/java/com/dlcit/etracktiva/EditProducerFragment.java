package com.dlcit.etracktiva;

import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.dlcit.etracktiva.helper.LockableScrollView;
import com.dlcit.etracktiva.helper.UPA;

import org.osmdroid.api.IMapController;
import org.osmdroid.config.Configuration;
import org.osmdroid.events.MapEventsReceiver;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapController;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.MapEventsOverlay;
import org.osmdroid.views.overlay.Marker;
import org.osmdroid.views.overlay.Overlay;

import java.util.ArrayList;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;
import static java.lang.Boolean.FALSE;

public class EditProducerFragment extends Fragment{
    private SimpleCursorAdapter dataAdapter;
    private SQLiteDatabase db;
    private MyDatabase mydb;
    private String farmerid;
    private int why_i_come;
    private static String STRING_EMPTY = "";
    private FloatingActionButton searchbtn;
    ListView listView;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        why_i_come = getArguments().getInt("why_i_come");
        return inflater.inflate(R.layout.fragment_edit_producer, container, false);
    }
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mydb = new MyDatabase(getContext(),"db_lote",null,1);
        db = mydb.getWritableDatabase();
        searchbtn = (FloatingActionButton)view.findViewById(R.id.btnsearchfarmer);
        searchbtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                searchfarmer();
            }
        });
        Boolean tableexists = mydb.isTableExists("farmers",db);
        listView = (ListView) view.findViewById(R.id.listvieweditproducer);
        if (FALSE.equals(tableexists)){
            Toast.makeText(getContext(), "Aun no existen datos!!!", Toast.LENGTH_LONG).show();
            return;
        }
        populatelistview("all","");
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> listView, View view,
                                    int position, long id) {
                Cursor cursor = (Cursor) listView.getItemAtPosition(position);
                String idfarmer =
                        cursor.getString(cursor.getColumnIndexOrThrow("unique_farmer_id"));
                farmerid = idfarmer;
                switch (why_i_come) {
                    case 2:
                        // I am here just for select the farmer, go back with the national id number
                        String cedula = cursor.getString(cursor.getColumnIndexOrThrow("idcard"));
                        // Get all upas
                        List<UPA> upasthisfarmers = mydb.getUPAsfromFarmer(db, cedula);
                        String[] datafarmer = new String[1 + upasthisfarmers.size()];
                        datafarmer[0] = cedula;
                        for (int i = 1; i < upasthisfarmers.size() + 1 ; i++) {
                            String keyname = upasthisfarmers.get(i - 1).getKeyname();
                            String id_ = upasthisfarmers.get(i - 1).getID();
                            String key_w_id = keyname + "%" + id_;
                            datafarmer [i] = key_w_id;
                        }
                        // put data into serialize and comeback
                        Intent intent = new Intent(getActivity(), Recepcion.class);
                        intent.putExtra("frgToLoad",1);
                        intent.putExtra("farmerdata",datafarmer);
                        startActivity(intent);
                        return;
                }
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setMessage("Eliga opcion para el productor " + idfarmer).setPositiveButton("Ver/Editar",
                        dialogClickListener2)
                        .setNegativeButton("Eliminar", dialogClickListener2).show();

            }
        });

    }
    DialogInterface.OnClickListener dialogClickListener2 = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            switch (which){ // We have internet, so we save both locally and remotely
                case DialogInterface.BUTTON_POSITIVE:
                    Intent intent = new Intent(getActivity(), AddProducerActivity.class);
                    intent.putExtra("frgToLoad", 1); // Value 1 to go to new register egress
                    String [] farmerData = mydb.getFarmer(db,farmerid);
                    intent.putExtra("farmerdata",farmerData);
                    startActivity(intent);
                    break;
                case DialogInterface.BUTTON_NEGATIVE:
                    // Case Eliminar lote
                    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                    builder.setMessage("¿Esta seguro de eliminar el productor " + farmerid + "?").setPositiveButton(
                            "Si", dialogClickListenerConfirmarDelete)
                            .setNegativeButton("No", dialogClickListenerConfirmarDelete).show();
                    break;
            }
        }
    };
    DialogInterface.OnClickListener dialogClickListenerConfirmarDelete = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            switch (which){
                case DialogInterface.BUTTON_POSITIVE:
                    mydb.deleteProducer(db,farmerid);
                    // Refresh fragment edit
                    refreshView();
                    break;
                case DialogInterface.BUTTON_NEGATIVE:
                    break;
            }
        }
    };

    private void refreshView(){
        // Launch fragment search and edit
        Fragment newFragment = new EditProducerFragment();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_addproducer_container, newFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private void searchfarmer(){
        // Pop-up dialog box for searching producer by last_name or cedula
        LayoutInflater li = LayoutInflater.from(getContext());
        View promptsView = li.inflate(R.layout.dialog_search_producer, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());
        alertDialogBuilder.setView(promptsView);
        alertDialogBuilder.setTitle("Ingrese datos para la busqueda");
        final EditText lastameinput = (EditText) promptsView.findViewById(R.id.searchlastnamefarmer);
        final EditText cedulainput = (EditText) promptsView.findViewById(R.id.searchcedulafarmer);

        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                String cedsearch = cedulainput.getText().toString();
                                String namesearch = lastameinput.getText().toString();
                                // Lets search  the producer and move the list view there
                                if (!STRING_EMPTY.equals(cedsearch)) {
                                    populatelistview("cedula",cedsearch);
                                    //int moveto = mydb.searchProducerbyCedula(db,cedsearch);
                                    //listView.smoothScrollToPosition(moveto+1);
                                    return;
                                }
                                if (!STRING_EMPTY.equals(lastameinput) && STRING_EMPTY.equals(cedsearch) ) {
                                    populatelistview("lastname",namesearch);
                                    return;
                                }

                                return;
                            }
                        })
                .setNegativeButton("Cancelar",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void populatelistview(String query, String value){
        String dbquery = new String();
        if ("all".equals(query)) {
            dbquery = "SELECT _id , unique_farmer_id, idcard , name, lastname , phone ,email,  " +
                    "datetime(timestamp, 'unixepoch') AS date_formatted " +
                    "FROM farmers ORDER BY timestamp DESC";
        }
        if ("cedula".equals(query)) {
            dbquery = "SELECT _id , unique_farmer_id, idcard , name, lastname , phone ,email,  " +
                    "datetime(timestamp, 'unixepoch') AS date_formatted " +
                    "FROM farmers WHERE idcard='"+ value + "'" + " ORDER BY timestamp DESC";
        }
        if ("lastname".equals(query)) {
            dbquery = "SELECT _id , unique_farmer_id, idcard , name, lastname , phone ,email,  " +
                    "datetime(timestamp, 'unixepoch') AS date_formatted " +
                    "FROM farmers WHERE lastname='"+ value + "'" + " COLLATE NOCASE ORDER BY timestamp DESC";
        }

        Cursor c = db.rawQuery(dbquery, null);

        String[] columns = new String[] {
                "idcard",
                "name",
                "lastname",
                "phone",
                "email",
                "date_formatted"
        };
        int[] to = new int[] {
                R.id.cedulafarmertxt,
                R.id.namefarmer,
                R.id.lastnamefarmer,
                R.id.telephonefarmer,
                R.id.emailfarmer,
                R.id.timestampfarmer,
        };
        dataAdapter = new SimpleCursorAdapter(getContext(),R.layout.farmer_info,c,columns,to,0);
        listView.setAdapter(dataAdapter);
    }

}

