package com.dlcit.etracktiva;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

import com.dlcit.etracktiva.helper.UPA;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.osmdroid.tileprovider.modules.SqliteArchiveTileWriter;

import java.nio.charset.IllegalCharsetNameException;
import java.util.ArrayList;
import java.util.List;

public class MyDatabase extends SQLiteOpenHelper {
    String sqlCreate = "CREATE TABLE IF NOT EXISTS registros (_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
            "unique_reg_id TEXT, lote_identificador TEXT, upa TEXT, producer TEXT, storage TEXT, " +
            "quantity REAL, type TEXT,variety TEXT, inen TEXT, cert TEXT, precio REAL, last_modif TEXT, " +
            "timezone TEXT, latitude REAL, longitude REAL, locality TEXT, " +
            "country TEXT, altitude REAL, weather TEXT, timestamp TEXT, serverID INTEGER)";

    public MyDatabase(Context contexto, String nombre,
                      SQLiteDatabase.CursorFactory factory, int version) {
        super(contexto, nombre, factory, version);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(sqlCreate);
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int versionAnterior, int versionNueva) {
        //NOTA: Por simplicidad del ejemplo aquí utilizamos directamente la opción de
        //      eliminar la tabla anterior y crearla de nuevo vacía con el nuevo formato.
        //      Sin embargo lo normal será que haya que migrar datos de la tabla antigua
        //      a la nueva, por lo que este método debería ser más elaborado.

        //Se elimina la versión anterior de la tabla
        db.execSQL("DROP TABLE IF EXISTS registros");
        //Se crea la nueva versión de la tabla
        db.execSQL(sqlCreate);
    }
    public void deleteTables(SQLiteDatabase db, String table){
        db.execSQL("DROP TABLE IF EXISTS " + table);
    }

    public void openConnection(SQLiteDatabase db) {
        db = this.getWritableDatabase();
    }

    public void closeConnection(SQLiteDatabase db) {
        db.close();
    }

    public void fecthLotes(SQLiteDatabase db){

    }

    public void deleteLote(SQLiteDatabase db, String lote_id){
        db.execSQL("DELETE FROM registros WHERE lote_identificador='" + lote_id+"'");
    }

    public String [] getRegister(SQLiteDatabase db, String loteid){
        Cursor c = db.rawQuery("SELECT unique_reg_id, last_modif FROM registros WHERE lote_identificador='" +
                loteid + "'", null);
        String [] array_reg = new String[c.getCount()];
        int j = 0;
        if (c.moveToFirst()){
            do{
                String reg_ = c.getString(c.getColumnIndex("unique_reg_id"));
                String lf = c.getString(c.getColumnIndex("last_modif"));
                array_reg[j] = reg_+" "+lf;
                j ++ ;
                // do what ever you want here
            }while(c.moveToNext());
        }
        c.close();
        return array_reg;
    }

    public String getLatestData(SQLiteDatabase db, String table){
        Cursor c = db.rawQuery("SELECT timestamp FROM " + table + " ORDER BY timestamp DESC", null);
        String last_data = "1980-11-11 11:11:11";// Just a very old date
        if (c.moveToFirst()){
            do{
                last_data = c.getString(c.getColumnIndex("last_modif"));
                break;
            }while(c.moveToNext());
        }
        return last_data;
    }
    public boolean isTableExists(String tableName,  SQLiteDatabase db) {
        Cursor cursor = db.rawQuery("select DISTINCT tbl_name from sqlite_master where tbl_name = '"+tableName+"'", null);
        if(cursor!=null) {
            if(cursor.getCount()>0) {
                cursor.close();
                return true;
            }
            cursor.close();
        }
        return false;
    }

    public void createTableProducers(SQLiteDatabase db){
        String sqlCreateProd = "CREATE TABLE IF NOT EXISTS farmers (_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "unique_farmer_id TEXT, idcard  TEXT, name TEXT, lastname TEXT, phone TEXT, " +
                "email TEXT, farmercode TEXT, farmertype TEXT, upas TEXT,  timestamp TEXT)";
        db.execSQL(sqlCreateProd);
    }

    public void createTableUPAs(SQLiteDatabase db){
        String sqlCreateProd = "CREATE TABLE IF NOT EXISTS upas (_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "unique_upa_id TEXT, keynameupa TEXT, latitude  REAL, longitude REAL, variety TEXT, area REAL, " +
                "owner REAL, timestamp TEXT, serverID TEXT)";
        db.execSQL(sqlCreateProd);
    }
    public void deleteProducer(SQLiteDatabase db, String farmerid){
        db.execSQL("DELETE FROM farmers WHERE unique_farmer_id='" + farmerid+"'");
    }
    public int searchProducerbyCedula(SQLiteDatabase db, String cedula){
        //TODO: Do it efficiently!!!!!
        Cursor c = db.rawQuery("SELECT idcard FROM farmers ORDER BY timestamp DESC" ,null);
        int index = 0;
        if (c.moveToFirst()){
            do{
                String cedul = c.getString(c.getColumnIndex("idcard"));
                int y =0 ;
                index ++;
                if (cedul.equals(cedula)){
                    return  index;
                }
                //break;
            }while(c.moveToNext());
        }
        return 0;
    }

    public String [] getFarmer(SQLiteDatabase db, String farmerid){
        Cursor c = db.rawQuery("SELECT  idcard , name , lastname , phone ,email, farmercode, farmertype,unique_farmer_id " +
                " FROM farmers WHERE unique_farmer_id='" +
                farmerid + "'", null);
        String [] array_farmer = new String[c.getColumnCount()];
        if (c.moveToFirst()){
            do{
                String idcard = c.getString(c.getColumnIndex("idcard"));
                String name = c.getString(c.getColumnIndex("name"));
                String lastname = c.getString(c.getColumnIndex("lastname"));
                String phone = c.getString(c.getColumnIndex("phone"));
                String email = c.getString(c.getColumnIndex("email"));
                String farmercode = c.getString(c.getColumnIndex("farmercode"));
                String farmertype = c.getString(c.getColumnIndex("farmertype"));
                String uniquefarmerid = c.getString(c.getColumnIndex("unique_farmer_id"));
                array_farmer[0] = idcard;
                array_farmer[1] = name;
                array_farmer[2] = lastname;
                array_farmer[3] = phone;
                array_farmer[4] = email;
                array_farmer[5] = farmercode;
                array_farmer[6] = farmertype;
                array_farmer[7] = uniquefarmerid;
                // do what ever you want here
            }while(c.moveToNext());
        }
        c.close();
        return array_farmer;
    }

    public List<UPA> getUPAsfromFarmer(SQLiteDatabase db, String cedula){
        Cursor c = db.rawQuery("SELECT  * from upas WHERE owner='" + cedula + "'", null);
        List<UPA> allupas = new ArrayList<UPA>();
        if (c.moveToFirst()){
            do{
               UPA currentUPA = new UPA();
               String uniqueupaid = c.getString(1);
               String keyname = c.getString(2);
               String lat = c.getString(3);
               String longit = c.getString(4);
               String variet = c.getString(5);
               String area = c.getString(6);
               String owner = c.getString(7);
               String timestamp = c.getString(8);
               currentUPA.setValues(lat,longit,uniqueupaid,variet,timestamp,area,keyname,owner);
               allupas.add(currentUPA);
            }while(c.moveToNext());
        }
        c.close();
        return allupas;
    }
    public Boolean unique_Id_was_inserted(SQLiteDatabase db, String cedula){
        Cursor c = db.rawQuery("SELECT  * from farmers WHERE unique_farmer_id='" + cedula + "'", null);
        if (c.getCount()>0){
            return Boolean.TRUE;
        }
        else{
            return Boolean.FALSE;
        }
    }
    public void insertServerID(SQLiteDatabase db , String table, String serverid_, String uniqueupa_id, String namecolumnuniqueid){
        db.execSQL("UPDATE " + table +" SET serverID='" + serverid_ + "'" + "WHERE " + namecolumnuniqueid + "='" + uniqueupa_id + "'");
    }

    public String getServerId(SQLiteDatabase db, String table,  String [] conditionvalues, String [] conditioncolumns){
        // Lets assemble the query
        String whereqry = " WHERE ";
        for (int i = 0 ; i < conditioncolumns.length ; i ++){
            whereqry += conditioncolumns[i];
            whereqry += "='";
            whereqry += conditionvalues[i];
            whereqry += "'";
            if (i < conditioncolumns.length-1) {
                whereqry += " AND ";
            }
        }
        Cursor c = db.rawQuery("SELECT serverID from " + table + whereqry, null);
        String idserver = "-1";
        if (c.moveToFirst()){
            do{
                if (c.getCount()==1){
                    idserver = c.getString(0);
                    break;
                 }
                 else{
                     idserver += "%";
                     idserver += c.getString(0);
                 }
            }while(c.moveToNext());
        }
        return idserver;
    }

    public int saveLot(SQLiteDatabase db, JSONObject server_resp, JSONArray values){
        String sqlCreate = "CREATE TABLE IF NOT EXISTS lots (_id INTEGER PRIMARY KEY AUTOINCREMENT, loteid INTEGER, registerids TEXT)";
        db.execSQL(sqlCreate);
        try {
            String resp_code = server_resp.get("response_code").toString();
            String serverID = server_resp.get("msg_body").toString();
            if ("201".equals(resp_code)) {
                serverID = serverID.replace("\n", "");
                int sid = Integer.parseInt(serverID);
                String reg_chain= "";
                for (int i = 0; i < values.length(); i++) {
                    Object currval = values.get(i);
                    reg_chain += "%" + currval.toString();
                }
                reg_chain = reg_chain.substring(1);
                ContentValues newLot = new ContentValues();
                newLot.put("loteid", sid);
                newLot.put("registerids",reg_chain);
                long u = db.insert("lots", null, newLot);
                if (u != -1){
                    return 1;
                }
                else { return 0 ;}
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return 0;
    }
    public int getServerIDLot(SQLiteDatabase db, JSONArray regvalues){
        try{
            String reg_chain= "";
            for (int i = 0; i < regvalues.length(); i++) {
                Object currval = regvalues.get(i);
                reg_chain += "%" + currval.toString();
            }
            reg_chain = reg_chain.substring(1);
            String sqlqry = "SELECT loteid FROM lots WHERE registerids='" + reg_chain +"'" ;
            Cursor cursor = db.rawQuery(sqlqry,null);
            if (cursor.moveToFirst()){
                do{
                    String serveridlote  = cursor.getString(0);
                    int y = Integer.parseInt(serveridlote);
                    return y;
                }while(cursor.moveToNext());
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return -1;
    }

}