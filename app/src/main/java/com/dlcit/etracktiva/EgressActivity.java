package com.dlcit.etracktiva;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.FrameLayout;


public class EgressActivity extends AppCompatActivity   {

    private FrameLayout mFrameLayoutPrincipal;
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment selectedFragment = null;
            //selectedFragment = new NewFragment();
            switch (item.getItemId()) {
                case R.id.navigation_register_egress:
                   selectedFragment = new NewEgressFragment();
                   getSupportFragmentManager().beginTransaction().replace(R.id.fragment_egress_container,
                            selectedFragment).commit();
                    return true;
                case R.id.navigation_edit_egress:
                    return true;

            }
            //getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
            //      selectedFragment).commit();
            return false;
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_egress);
        mFrameLayoutPrincipal = (FrameLayout)findViewById(R.id.fragment_egress_container);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation_egress);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        int intentFragment = getIntent().getExtras().getInt("frgToLoad");
        Fragment selectedFragment = null;
        switch (intentFragment){
            case 1: // Fragment Camera
                selectedFragment = new NewEgressFragment();
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_egress_container,
                        selectedFragment).commit();
                break;
            case 2: // Fragment See Registers
                break;

        }
    }

}
