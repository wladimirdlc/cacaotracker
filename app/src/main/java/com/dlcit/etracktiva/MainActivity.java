package com.dlcit.etracktiva;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.dlcit.etracktiva.helper.CheckNetworkStatus;

public class MainActivity extends AppCompatActivity {


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment selectedFragment = null;
            //selectedFragment = new NewFragment();
            switch (item.getItemId()) {
                case R.id.sync_main:
                    if (CheckNetworkStatus.isNetworkAvailable(MainActivity.this)) {
                        UploaderHelper up = new UploaderHelper();
                        up.uploadAll(MainActivity.this);
                    }
                    else{
                        Toast.makeText(MainActivity.this,
                                "No se puede sincronizar sin conexion a Internet!!!",Toast.LENGTH_LONG).show();

                    }
                    return true;
                case R.id.config_main:
                    // TODO: Add all setting functions needed
                    Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
                    startActivity(intent);
                    return true;
                case R.id.help_main:
                    break;
                    //return true;
            }
            //getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
            //      selectedFragment).commit();
            return false;
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        Button btnrec = findViewById(R.id.btn_rec);
        Button btnproc = findViewById(R.id.btn_proc);
        Button btnegress= findViewById(R.id.btn_del);
        Button btnaddproducer = findViewById(R.id.btnaddproductores);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation_main_bar);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);


        btnrec.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Set wheather data fetching as background task to be done each time is possible
                Intent intent = new Intent(MainActivity.this, Recepcion.class);
                intent.putExtra("frgToLoad", 1); // Value 1 to go to new register fragment
                startActivity(intent);
            }
        });

        btnproc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, ProcessingActivity.class);
                intent.putExtra("lote_id","No seleccionado");
                intent.putExtra("frgToLoad", 1); // Value 1 to go to new register camera
                startActivity(intent);
            }
        });

        btnegress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, EgressActivity.class);
                intent.putExtra("frgToLoad", 1); // Value 1 to go to new register egress
                startActivity(intent);
            }
        });


        btnaddproducer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, AddProducerActivity.class);
                intent.putExtra("frgToLoad", 1); // Value 1 to go to new register egress
                startActivity(intent);
            }
        });


    }

}
