package com.dlcit.etracktiva;

import android.app.Activity;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.hardware.Camera;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.text.TextUtils;

import static android.content.Context.MODE_PRIVATE;

import net.sourceforge.zbar.Config;
import net.sourceforge.zbar.Image;
import net.sourceforge.zbar.ImageScanner;
import net.sourceforge.zbar.Symbol;
import net.sourceforge.zbar.SymbolSet;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;

public class ProcessingCameraFragment extends Fragment implements Camera.PreviewCallback, ZBarConstants   {
    private Spinner mSpinnerTipoProc;
    private Button mButtonDelLast;
    private static final String TAG = "ZBarScannerActivity";
    private CameraPreview mPreview;
    private Camera mCamera;
    private ImageScanner mScanner;
    private Handler mAutoFocusHandler;
    private boolean mPreviewing = true;
    private String uniqueloteid;
    private TextView mSearchLote;
    private TextView mTextViewLote;
    private TextView mTextViewQuantity;
    private TextView mSetProcessingDescription;
    private String username;
    private static String STRING_EMPTY = "";
    private static String STRING_NON_SELECT = "--Seleccione--";

    // Data to be inserted in the database -----> table procesos
    private String uniqueprocessingid;
    private String QRid;
    private String loteidentificador;
    private String type;
    private String process;
    private String  quantity;
    private String cert;
    private String last_modif;
    private String time_zone;

    // Variables to be used in the control of processed
    private Integer currentproccesed;
    private TextView mtextViewcurrentproccesed;
    private TextView mtextViewallprocessedinlote;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        uniqueloteid = getArguments().getString("lote_id");
        currentproccesed = 0;
        return inflater.inflate(R.layout.fragment_camera, container, false);
    }
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mSpinnerTipoProc = (Spinner)view.findViewById(R.id.spinnerTipoProc);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(),
                R.array.tipo_proc_array, android.R.layout.simple_spinner_item);
        mSpinnerTipoProc.setAdapter(adapter); //Setting this stupid stuff programatically!!
        mSpinnerTipoProc.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                ((TextView) parentView.getChildAt(0)).setTextColor(getResources().getColor(R.color.colorWhiteText));
                ((TextView) parentView.getChildAt(0)).setTextSize(12);
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) { }
        });

        mButtonDelLast = (Button)view.findViewById(R.id.btndellast);
        mSearchLote = (Button)view.findViewById(R.id.btnbuscarloteprocesin);
        mTextViewLote = (TextView)view.findViewById(R.id.txtLoteproc);
        mTextViewQuantity = (TextView)view.findViewById(R.id.txtpesoproc);
        mSetProcessingDescription = (Button)view.findViewById(R.id.btnprocdetails);
        mtextViewcurrentproccesed = (TextView)view.findViewById(R.id.txtscanned);
        mtextViewallprocessedinlote = (TextView)view.findViewById(R.id.txttotalscannedinlote);
        mTextViewLote.setText(uniqueloteid);
        SharedPreferences prefs = getContext().getSharedPreferences("LOGIN_DATA", MODE_PRIVATE);
        username = (prefs.getString("user", "Guest"));
        MyDatabaseProcessing mydb = new MyDatabaseProcessing(getContext(),"db_lote",null,1);
        SQLiteDatabase db = mydb.getWritableDatabase();
        if (uniqueprocessingid!=null) {
            mtextViewallprocessedinlote.setText(mydb.getProcessedinLote(db, uniqueloteid));
        }
        //CAMERA
        if(!isCameraAvailable()) {
            // Cancel request if there is no rear-facing camera.
            cancelRequest();
            return;
        }
        // Hide the window title.
        //requestWindowFeature(Window.FEATURE_NO_TITLE);
        // getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        mAutoFocusHandler = new Handler();
        // Create and configure the ImageScanner;
        setupScanner();
        // Create a RelativeLayout container that will hold a SurfaceView,
        // and set it as the content of our activity.
        mPreview = new CameraPreview(getContext(), this, autoFocusCB);
        //setContentView(mPreview);
        // Create our Preview view and set it as the content of our activity.
        FrameLayout preview = (FrameLayout) view.findViewById(R.id.camera_preview);
        preview.addView(mPreview);
        mButtonDelLast.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setMessage("¿Está seguro de eliminar el ultimo registro?").setPositiveButton
                        ("Si", acceptdeletlistener)
                        .setNegativeButton("No", acceptdeletlistener).show();

            }
        });
        mSearchLote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // I need to go to the activity reception and fragment edit
                Intent intent = new Intent(getActivity(), Recepcion.class);
                intent.putExtra("frgToLoad", 2);
                startActivity(intent);
            }
        });

        mSetProcessingDescription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // TODO: Display an dialog box for inserting all description
            }
        });
    }

    public void setupScanner() {
        mScanner = new ImageScanner();
        mScanner.setConfig(0, Config.X_DENSITY, 3);
        mScanner.setConfig(0, Config.Y_DENSITY, 3);

        int[] symbols = getActivity().getIntent().getIntArrayExtra(SCAN_MODES);
        if (symbols != null) {
            mScanner.setConfig(Symbol.NONE, Config.ENABLE, 0);
            for (int symbol : symbols) {
                mScanner.setConfig(symbol, Config.ENABLE, 1);
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        // Open the default i.e. the first rear facing camera.
        mCamera = Camera.open();
        mCamera.setDisplayOrientation(90);
        if(mCamera == null) {
            // Cancel request if mCamera is null.
            cancelRequest();
            return;
        }
        mPreview.setCamera(mCamera);
        mPreview.showSurfaceView();
        mPreviewing = true;
        MyDatabaseProcessing mydb = new MyDatabaseProcessing(getContext(),"db_lote",null,1);
        SQLiteDatabase db = mydb.getWritableDatabase();
        if (!"No seleccionado".equals(uniqueloteid)) {
            mtextViewallprocessedinlote.setText(mydb.getProcessedinLote(db, uniqueloteid));
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        // Because the Camera object is a shared resource, it's very
        // important to release it when the activity is paused.
        if (mCamera != null) {
            mPreview.setCamera(null);
            mCamera.cancelAutoFocus();
            mCamera.setPreviewCallback(null);
            mCamera.stopPreview();
            mCamera.release();
            mPreview.hideSurfaceView();
            mPreviewing = false;
            mCamera = null;
        }
    }

    public boolean isCameraAvailable() {
        PackageManager pm = getActivity().getPackageManager();
        return pm.hasSystemFeature(PackageManager.FEATURE_CAMERA);
    }

    public void cancelRequest() {
        Intent dataIntent = new Intent();
        dataIntent.putExtra(ERROR_INFO, "Camera unavailable");
        getActivity().setResult(Activity.RESULT_CANCELED, dataIntent);
        getActivity().finish();
    }

    public void onPreviewFrame(byte[] data, Camera camera) {
        if ("No seleccionado".equals(uniqueloteid)){
            Toast.makeText(getContext(),"Eliga primero el Lote!!!", Toast.LENGTH_SHORT).show();
            return;
        }
        mtextViewcurrentproccesed.setText(currentproccesed.toString());
        Camera.Parameters parameters = camera.getParameters();
        Camera.Size size = parameters.getPreviewSize();
        Image qrcode = new Image(size.width, size.height, "Y800");
        qrcode.setData(data);
        int result = mScanner.scanImage(qrcode);
        if (result != 0) {
            onPause();
            //mCamera.cancelAutoFocus();
            //mCamera.setPreviewCallback(null);
            //mCamera.stopPreview();
            //mPreviewing = false;
            SymbolSet syms = mScanner.getResults();
            for (Symbol sym : syms) {
                String symData = sym.getData();
                if (!TextUtils.isEmpty(symData)) {
                    String[] qrdata = symData.split("%");
                    if (qrdata.length == 3) {
                        if (getString(R.string.dlcit).equals(qrdata[1])) {
                            QRid = qrdata[2];
                            processQR(QRid);
                        }
                        else{showInvalidQR();}
                    }
                    else {showInvalidQR();}
                }
                else{
                    showInvalidQR();
                }
            }
        }
    }
    private Runnable doAutoFocus = new Runnable() {
        public void run() {
            if(mCamera != null && mPreviewing) {
                mCamera.autoFocus(autoFocusCB);
            }
        }
    };

    // Mimic continuous auto-focusing
    Camera.AutoFocusCallback autoFocusCB = new Camera.AutoFocusCallback() {
        public void onAutoFocus(boolean success, Camera camera) {
            mAutoFocusHandler.postDelayed(doAutoFocus, 1000);
        }
    };
    private void processQR(String qrtext){
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){ //
                    case DialogInterface.BUTTON_POSITIVE:
                        // Processing and resume
                        // Save in database
                        currentproccesed ++;
                        mtextViewcurrentproccesed.setText(currentproccesed.toString());
                        saveInDB();
                        onResume();
                        break;
                    case DialogInterface.BUTTON_NEGATIVE:
                        // Only resume
                        onResume();
                        break;
                }
            }
        };
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMessage("¿Está seguro de ingresar este codigo QR " + QRid + "?").setPositiveButton("Si", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();
    }
    protected void showInvalidQR(){
        Toast.makeText(getContext(),"QR Code no valido!!!", Toast.LENGTH_LONG).show();
        SystemClock.sleep(500);
        onResume();
    }
    private String getProcessId(String whoami){
        Long tsLong = System.currentTimeMillis()/1000;
        String ts = tsLong.toString();
        return whoami + "_P_" + ts;
    }

    private void saveInDB(){
        MyDatabaseProcessing mydb = new MyDatabaseProcessing(getContext(),"db_lote",null,1);
        SQLiteDatabase db = mydb.getWritableDatabase();
        mydb.onCreate(db);
        if(db != null) {
            uniqueprocessingid = getProcessId(username);
            loteidentificador = mTextViewLote.getText().toString();
            type = mSpinnerTipoProc.getSelectedItem().toString();
            process = "TODO: provisional"; //TODO: get real processing description
            quantity =  mTextViewQuantity.getText().toString();
            cert = "TODO: provisional"; //TODO: get real processing description
            DateFormat date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            TimeZone timeZone = TimeZone.getDefault();
            last_modif = date.format(Calendar.getInstance().getTime());
            time_zone = timeZone.getDisplayName(false,TimeZone.SHORT);
            Long tsLong = System.currentTimeMillis()/1000;
            String ts =tsLong.toString();
            // checking any empty field
            if (STRING_NON_SELECT.equals(mSpinnerTipoProc.getSelectedItem().toString())){type="N.A";}
            if (STRING_EMPTY.equals(mTextViewQuantity.getText().toString())){quantity="0";}
            // inserting into the table
            ContentValues newProcess = new ContentValues();
            newProcess.put("unique_process_id",uniqueprocessingid);
            newProcess.put("qrcode",QRid);
            newProcess.put("lote_identificador",loteidentificador);
            newProcess.put("type",type);
            newProcess.put("process",process);
            newProcess.put("quantity",quantity);
            newProcess.put("cert",cert);
            newProcess.put("last_modif",last_modif);
            newProcess.put("timezone",time_zone);
            newProcess.put("timestamp",ts);
            newProcess.put("serverID","-1");
            long u = db.insert("procesos", null, newProcess);
            if (u != -1){
                Toast.makeText(getContext(), "Proceso: " + uniqueprocessingid + " guardado localmente!!!", Toast.LENGTH_LONG).show();
            }
            else {
                Toast.makeText(getContext(), "Algo salio mal, registro no ingresado", Toast.LENGTH_LONG).show();
            }
        }
        // Update number of processed items in this lote
        mydb.getProcessedinLote(db,loteidentificador);
        db.close();
    }

    DialogInterface.OnClickListener acceptdeletlistener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            if (currentproccesed<1){
                return;
            }
            switch (which){ // We have internet, so we save both locally and remotely
                case DialogInterface.BUTTON_POSITIVE:
                    // I need to delete the last register inserted into the database and under this lote
                    MyDatabaseProcessing mydb = new MyDatabaseProcessing(getContext(),"db_lote",null,1);
                    SQLiteDatabase db = mydb.getWritableDatabase();
                    mydb.deleteLastProcess(db,mTextViewLote.getText().toString());
                    if (currentproccesed > 0){
                        currentproccesed --;
                        mtextViewcurrentproccesed.setText(currentproccesed.toString());
                    }
                    mtextViewallprocessedinlote.setText(mydb.getProcessedinLote(db,uniqueloteid));
                    break;
                case DialogInterface.BUTTON_NEGATIVE:

                    break;
            }
        }
    };

}
