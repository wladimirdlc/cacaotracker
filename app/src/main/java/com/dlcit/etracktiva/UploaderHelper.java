package com.dlcit.etracktiva;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.widget.Toast;

import com.dlcit.etracktiva.helper.HttpJsonParser;
import com.dlcit.etracktiva.helper.HttpJsonParser_new;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;
import static java.lang.Boolean.FALSE;

public class UploaderHelper {
    private MyDatabase mydbreg;
    private SQLiteDatabase db;
    private Context context;
    private ProgressDialog pDialog;
    private static final String KEY_SUCCESS = "success";
    private int success;
    private int successcumul;
    private int bigsucess;
    private String last_synced;

    //private static final String BASE_URL = "https://lotes.000webhostapp.com/lotes/"; //my webserver
    private static final String BASE_URL = "https://api.cocoa.sasan.lu/";
    List<String[]> alldata;
    List<String> allKeys ;

    private void setDB(Context c){
        mydbreg = new MyDatabase(c,"db_lote",null,1);
        db = mydbreg.getWritableDatabase();
        context = c;
        successcumul = 0;
        SharedPreferences prefs = context.getSharedPreferences("SYNC_DATA", MODE_PRIVATE);
        last_synced = (prefs.getString("last_sync", "0"));
        int y = 0;
    }

    public void uploadAll(Context context) {
        setDB(context);
        // Start Asycn process uploading all
        new AddLoteAsyncTaskUpdate().execute();
    }
    private class AddLoteAsyncTaskUpdate extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //Display proggress bar
            pDialog = new ProgressDialog(context);
            pDialog.setMessage("Sincronizando... Espere por favor!!!");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            bigsucess = 0;
            for (int k = 0 ; k < 6; k ++) { // One k per table, this should be replaced by a table with the names
                String script_php = "";
                if (k == 0){
                    // I will add farmers
                    populateKeyFarmers();
                    setAlldata(db,"farmers","name,lastname,phone,idcard,email,farmertype,farmercode, _id");
                    script_php = "farmers"; // Farmers does not need extra server ID for control because cedula is unique and server refuses duplicated entries
                }
                if (k == 1){
                    populateKeyUPAs();
                    setAlldata(db,"upas", "latitude,longitude, CAST(CAST(owner AS FLOAT) AS bigint) ,area,keynameupa,variety,unique_upa_id,serverID"); // I cast owner because I stored it as REAL
                    script_php = "upas";

                }
                if (k == 2){
                    populateKeyRegistros();
                    setAlldata(db,"registros","quantity, producer,upa, precio,type,cert,_id, serverID"); // ALWAYS place unique id value in the second latest element
                    script_php = "registers";
                }
                if (k == 3){
                    populateKeyLots();
                    setLotdata(db);
                    script_php = "lots";

                }
                if (k == 4){
                    populateKeyProcesos();
                    setAlldata(db,"procesos", "lote_identificador, quantity, type, qrcode,_id, serverID ");
                    script_php = "processes";
                }

                if (k == 5){
                    populateKeyExits();
                    setAlldata(db,"egresos", "client_id, typeclient, ammount_paid, typeproduct,qrcode,_id, serverID ");
                    script_php = "exits";
                }

                for (int j = 0; j < alldata.size(); j++) {//Saving all registers
                    //HttpJsonParser httpJsonParser = new HttpJsonParser();
                    Map<String, String> httpParams = new HashMap<>();
                    JSONObject httpJSONParam = new JSONObject();
                    String[] current_data = alldata.get(j);

                    // For registers I need the server upa ID
                    if ("registers".equals(script_php)){
                        // I need to get the previously upa id assigned to the upa of this reg and replace it
                        String [] conditions_values = {current_data[1],current_data[2].split("%")[0]};
                        String [] conditions_columns = {"owner", "keynameupa"};
                        String upa_id_server = getServerID("upas",conditions_values, conditions_columns);
                        // Now replace the upa with the server upa id vefore uploading
                        current_data[2]=upa_id_server;
                    }

                    // For processes I need the server lot ID previously saved
                    if ("processes".equals(script_php)){
                        // I need to get the previously saved serverID for the corresponding lot
                        // First with the local lote_id, lets get the server ID of the registers
                        String [] conditions_values = {current_data[0]};
                        String [] conditions_columns = {"lote_identificador"};
                        String regs_serverid = getServerID("registros",conditions_values, conditions_columns);
                        regs_serverid = regs_serverid.substring(3); // I do not need the first two element
                        String regsids [] = regs_serverid.split("%");
                        // Now lets find the lot id based on the saved regs
                        JSONArray val = new JSONArray();
                        for (int h = 0 ; h < regsids.length; h ++) {
                            val.put(Integer.parseInt(regsids[h]));
                        }
                        int serveridlot = mydbreg.getServerIDLot(db,val);
                        // Now replace the local lot id with the respective server id
                        current_data[0] = Integer.toString(serveridlot);
                    }

                    // First check the last element, which ALWAYS!!!! MUST be the corresponding serverID
                    int serverID = Integer.parseInt(alldata.get(j)[alldata.get(j).length - 1 ]);
                    if ("upas".equals(script_php) && serverID > 0){
                        continue;
                    }
                    if ("registers".equals(script_php) && serverID > 0){
                        continue;
                    }
                    if ("processes".equals(script_php) && serverID > 0){
                        continue;
                    }
                    if ("exits".equals(script_php) && serverID > 0){
                        continue;
                    }
                    if ("lots".equals(script_php)){
                        // This is special
                        JSONArray val = new JSONArray();
                        for (int h = 0 ; h < current_data.length; h ++) {
                            val.put(Integer.parseInt(current_data[h]));
                        }
                        try {
                            httpJSONParam.put("registers", val);
                        }catch (Exception e){}
                        HttpJsonParser_new httpjsonP = new HttpJsonParser_new();
                        if(mydbreg.isTableExists("lots",db)) {
                            int serveridlot = mydbreg.getServerIDLot(db, val);
                            if (serveridlot > 0) {
                                continue;
                            }
                        }
                        JSONObject resp = httpjsonP.makeHttpRequest(BASE_URL + script_php,"POST",httpJSONParam);
                        // Save table of lots and registers (ALL are server IDs)
                        mydbreg.saveLot(db,resp,val);
                        continue;
                    }

                    //Populating request parameters
                    for (int i = 0; i < allKeys.size(); i++) {
                        httpParams.put(allKeys.get(i), current_data[i]);
                        try {
                            httpJSONParam.put(allKeys.get(i), current_data[i]);
                        }catch (Exception e){}
                    }
                    HttpJsonParser_new httpjsonP = new HttpJsonParser_new();
                    JSONObject resp = httpjsonP.makeHttpRequest(BASE_URL + script_php,"POST",httpJSONParam);
                    if ("upas".equals(script_php)) {
                        saveserverID(resp, alldata.get(j),"upas","unique_upa_id"); // I add the response as server ID into my local db
                    }
                    if ("registers".equals(script_php)) {
                        saveserverID(resp, alldata.get(j),"registros","_id"); // I add the response as server ID into my local db
                    }
                    if ("processes".equals(script_php)){
                        saveserverID(resp,alldata.get(j),"procesos","_id");
                    }
                    if ("exits".equals(script_php)){
                        saveserverID(resp,alldata.get(j),"egresos","_id");
                    }
                }

            }
            return null;

        }

        protected void onPostExecute(String result) {
            pDialog.dismiss();
            Activity activity = (Activity) context;
            activity.runOnUiThread(new Runnable() {
                public void run() {
                    if (bigsucess >= 0) { //Six is received when all 3 tables where correctly uploaded
                        //Display success message
                        Toast.makeText(context,"Datos sincronizados correctamente!!!", Toast.LENGTH_LONG).show();
                        // Set last sync to avoid future duplicates on the remote database
                        // Since we have server_id for checking this is outdated
                        /*long secs = (new Date().getTime())/1000;
                        String lastmodif = Long.toString(secs);
                        SharedPreferences.Editor editor = context.getSharedPreferences("SYNC_DATA", MODE_PRIVATE).edit();
                        editor.putString("last_sync", lastmodif);
                        editor.apply();*/
                    }else if(bigsucess == -1){
                        Toast.makeText(context,"No  hay nuevos datos para sincronizar!!!", Toast.LENGTH_SHORT).show();
                    }
                    else {
                        Toast.makeText(context,"Uno o mas registros no fueron sincronizados!!!",
                                Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }

    private void setAlldata(SQLiteDatabase db, String table, String fields){
        alldata = new ArrayList<String[]>();
        // First check if table exists !!!!
        Boolean tableexists = mydbreg.isTableExists(table,db);
        if (FALSE.equals(tableexists)){
            alldata = new ArrayList<String[]>();
            return;
        }
        // I have to select only data newer than the last_sync data
        Cursor cur ;
        if ("".equals(fields)) {
             cur = db.rawQuery("SELECT * FROM " + table + " WHERE timestamp>'" + last_synced + "'", null);
        }
        else{
            cur = db.rawQuery("SELECT _id,"+ fields + " FROM " + table + " WHERE timestamp>'" + last_synced + "'", null);
        }
        //Cursor cur = db.rawQuery("SELECT * FROM "+ table , null);
        if (cur.moveToFirst()) {
            do {
                String[] thisrow = new String[cur.getColumnCount() - 1];
                for (int i = 1; i < cur.getColumnCount(); i++) {
                    String column = cur.getColumnName(i);
                    String datacolumn = cur.getString(i);
                    thisrow[i - 1] = datacolumn;
                }
                alldata.add(thisrow);
            } while (cur.moveToNext());
        }
        cur.close();
    }

    private void setLotdata (SQLiteDatabase db){
        alldata = new ArrayList<String[]>();
        // First check if table exists !!!!
        Boolean tableexists = mydbreg.isTableExists("registros",db);
        if (FALSE.equals(tableexists)){
            alldata = new ArrayList<String[]>();
            return;
        }
        Cursor c2 = db.rawQuery("SELECT lote_identificador FROM registros GROUP BY lote_identificador", null);
        List<String> lotesid = new ArrayList<String>();
        if (c2.moveToFirst()) {
            do {
                String loteid = c2.getString(0);
                lotesid.add(loteid);
            } while (c2.moveToNext());
        }
        for (int i = 0 ; i < lotesid.size();i++){
            String currentlotid = lotesid.get(i);
            Cursor cur = db.rawQuery("SELECT serverID FROM registros WHERE lote_identificador='" + currentlotid + "'", null);
            String [] regInThisLote = new String[cur.getCount()];
            int j = 0 ;
            if (cur.moveToFirst()) {
                do {
                    String sid = cur.getString(0);
                    regInThisLote[j] = sid;
                    j ++ ;
                } while (cur.moveToNext());
                alldata.add(regInThisLote);
            }
            cur.close();
        }

    }
    private void populateKeyRegistros(){
        allKeys = new ArrayList<String>();
        allKeys.add("quantity");
        allKeys.add("national_id");
        allKeys.add("upa_id");
        allKeys.add("price");
        allKeys.add("product_type");
        allKeys.add("certification");
        allKeys.add("variety");

    }
    private void populateKeyProcesos(){
        allKeys = new ArrayList<String>();
        allKeys.add("lot_id");
        allKeys.add("weight");
        allKeys.add("type_");
        allKeys.add("qr_code");
        /*allKeys.add("process");
        allKeys.add("quantity");
        allKeys.add("cert");
        allKeys.add("last_modif");
        allKeys.add("timezone");
        allKeys.add("timestamp");*/

    }

    private void populateKeyExits(){
        allKeys = new ArrayList<String>();
        allKeys.add("name");
        allKeys.add("type_");
        allKeys.add("paid_amount");
        allKeys.add("payment_type");
        allKeys.add("qr_code");
    }

    private void populateKeyFarmers(){
        allKeys = new ArrayList<String>();
        allKeys.add("firstname");
        allKeys.add("lastname");
        allKeys.add("telephone");
        allKeys.add("national_id");
        allKeys.add("email");
        allKeys.add("type_");
        allKeys.add("code");
}
    private void populateKeyUPAs(){
        allKeys = new ArrayList<String>();
        allKeys.add("latitude");
        allKeys.add("longitude");
        allKeys.add("national_id");
        allKeys.add("farm_area_total");
        allKeys.add("name");
        allKeys.add("variety");
    }

    private void populateKeyLots(){
        allKeys = new ArrayList<String>();
        allKeys.add("registers");
    }
    private long getTimeDiff(String last_register_time, String last_synced_time){
        DateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date current_time = dateformat.parse(last_register_time);
            Date last_synced_ = dateformat.parse(last_synced_time);
            long diffInMillies = (current_time.getTime() - last_synced_.getTime());
            return diffInMillies; // if > 0 the current is greater than last_synced
        }catch (Exception e){}
        return 0;
    }
    private void saveserverID(@NotNull JSONObject response, String [] currentdata, String table, String uniquefieldname){
        try {
            String rcode = (String) response.getString("response_code");
            if ("201".equals(rcode)){ // Lets save server id on the upa local database
                String server_id = (String) response.get("msg_body");
                server_id = server_id.replace("\n", "");
                mydbreg.insertServerID(db,table, server_id,currentdata[currentdata.length-2],uniquefieldname); // The unique id value MUST be always placed in the last-2 position
                //mydbreg.insertUPAServerID(db,"-1",currentdata[currentdata.length-2]); //To reset serverIDs (only for DEBUG!!!)
                int y =0;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private String getServerID(String table, String [] conditions_value, String [] conditions_columns){
        return mydbreg.getServerId(db,table,conditions_value, conditions_columns);
    }

}
